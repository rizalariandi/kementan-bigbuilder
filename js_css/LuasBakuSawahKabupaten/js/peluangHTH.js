$(document).ready(function () {
    // var lineHujan = document.getElementById('lineHTHCanvas').getContext('2d');
    // window.lineHTH = new Chart(lineHujan, lineHTHConfig);
})

var hthJan = 7.60,
    hthFeb = 7.99,
    hthMar = 5.90,
    hthApr = 10.58;
var lineHTHConfig = {

    type: 'line',
    plugins: [ChartDataLabels],
    data: {
        labels: ["Apr-20", "Mei-20", "Jun-20"],//"Apr-20"],
        datasets: [{
            data: [hthJan, hthFeb, hthMar],//hthApr],
            label: "",
            borderColor: "#000000",
            fill: true,
            backgroundColor: "rgba(0,0,255,0.3)",
            datalabels: {
                display: true,
                anchor: 'end',
                align: 'top',
                backgroundColor: 'rgba(255,255,255,0.7)',
                formatter: function (value, context) {
                    return Number(Math.round(value * 100) / 100).toLocaleString("ID", { maximumFractionDigits: 2 });
                },
                display: 'auto'
            }

        }
        ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        tooltips: {
            enabled: false
        },
        legend: {
            display: false,
        },
        title: {
            display: true,
            text: ['Peluang Tanpa Hujan > 10 Hari', ''],
            fontSize: 20
        },
        scales: {
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: '%',
                    fontSize: 20
                },
                ticks: {
                    maxTicksLimit: 5,
                    callback: function (value, index, values) {
                        return value.toLocaleString("ID", { maximumFractionDigits: 1 });
                    }
                }
            }]
        }

    }
};