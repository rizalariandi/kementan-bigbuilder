var date_pick; // arr object untuk date & time
var zoom_view = 4.32;
var lft_map_highlight_kementan; // simpan openstreetmap tile
var json_data; // layer highlight untuk leaflet
var arr_fitbound_data = []; // berisi fitbound data dengan struktur: fitbound_provinsi, fitbound_kab, fitbound_kec
var select_input = []; // arr global berisi inputan dari select option
var data_prov_return = []; // arr berisi daftar provinsi dari json
var data_kabkot_return = []; // arr berisi daftar kabupaten-kota dari json
var data_kec_return = []; // arr berisi daftar kecamatan dari json
var selected_prov = "";
var selected_kab =""
var selected_kec =""
var data_tmp_kec = [];
var data = {};
var select_tahun = ""
var select_bulan = ""
var url_server_prov = "/assets/kementan/geojson_kementan/DB_PROP.json";
var url_server_kabkot = "/assets/kementan/geojson_kementan/DB_KAB.json";
var url_server_kecamatan = "/assets/kementan/geojson_kementan/DB_KEC.json";


$(function () {
    initMap();
    // set_prov();
    $(".m-selectpicker").selectpicker();
})

function changeIframe() {
    let addOn = (selected_prov ? "?provinsi=" + selected_prov : "") + (selected_kab ? "&kabupaten_kota=" + selected_kab : "") + (selected_kec ? "&kecamatan=" + selected_kec : "")
    addOn += (addOn.length > 0 ? "&" + select_tahun + (select_tahun ? "&" + select_bulan : select_bulan) : "?" + select_tahun + (select_tahun ? "&" + select_bulan : select_bulan))

    var detailUrl_poktan;
    if (!addOn.includes("provinsi") && !addOn.includes("kabupaten_kota") && !addOn.includes("kecamatan")) {
        detailUrl_poktan = "https://10.1.231.160:8443/public/dashboard/b109e4e9-9085-485c-af30-560e74275837" + addOn;
    } else if (addOn.includes("provinsi") && !addOn.includes("kabupaten_kota") && !addOn.includes("kecamatan")) {
        detailUrl_poktan = "https://10.1.231.160:8443/public/dashboard/333bf336-9138-44a6-87b8-4f5a36ae9261" + addOn;
    } else if (addOn.includes("kabupaten_kota") && !addOn.includes("kecamatan")) {
        detailUrl_poktan = "https://10.1.231.160:8443/public/dashboard/8d6cc0a4-f892-4078-8829-49496a2d0772" + addOn;
    } else if (addOn.includes("kecamatan")) {
        detailUrl_poktan = "https://10.1.231.160:8443/public/dashboard/e0554118-b0f8-462c-9274-809d778da6f6" + addOn;
    }
    // let detailUrl_poktan = "https://10.1.231.160:8443/public/dashboard/b109e4e9-9085-485c-af30-560e74275837" + addOn;

    let detailUrl_prod = "https://10.1.231.160:8443/public/dashboard/c539b776-c2bf-4693-961c-317902aacc30" + addOn;

    console.log("poktan = ", detailUrl_poktan);
    console.log("prod = ", detailUrl_prod);

    $("#iframe-poktan").attr("src", detailUrl_poktan);
    $("#iframe-produktivitas").attr("src", detailUrl_prod);
}


function removeOptions(ElemId, content) {
    var form_opt = "<option value='" + content + "'> " + content + " </option>";
    $("#" + ElemId).html(form_opt);
}

function getDate(content) {
    var tmp = content.value.split("-");
    date_pick = {
        "tanggal": tmp[2],
        "bulan": tmp[1],
        "tahun": tmp[0]
    }
    console.log(tmp, "+", date_pick);
    return date_pick;
}

function initMap() {
    const default_coord1 = ["Center", -2.600029, 118.015776];
    // leaflet init: id_html -> unntuk selector id tag di html
    // set_view -> untuk assign koordinat default ketika map di-load
    // zoom -> untuk set zoom level default ketika map di-load
    lft_map_highlight_kementan = L.map('map-highlight-2', {
        minZoom: 4
    }).setView([default_coord1[1], default_coord1[2]], zoom_view);

    // OpenStreetMap init
    const OpenStreetMapUrl = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";

    const OpenStreetMapTile2 = L.tileLayer(OpenStreetMapUrl, {
        minZoom: 1,
        maxZoom: 20,
        attribution: "<a href='http://www.openstreetmap.org'> &copy;OpenStreetMap </a>, <a href='http://www.pertanian.go.id'> Kementan </a>"
    });

    OpenStreetMapTile2.addTo(lft_map_highlight_kementan);

    // tambah layer untuk highlight
    json_data = L.geoJSON().addTo(lft_map_highlight_kementan);
}

// function getprov(result) {
//     console.log(result);
// }

function get_prov(result) {
    // var content = $("#select-prov").val();
    console.log("masuk get_prov");
    content = result.value.split(",")[0];
    console.log(content);
    selected_prov = content;
    selected_kab = ""
    selected_kec = ""    
    select_input = [];    
    select_input.push(content);
        // console.log("arr select = "+ select_input);
        // select_input = [];
        // select_input.push(content);
        $("#select-kabkota").val('default');
        $("#select-kabkota").selectpicker("refresh");        
        $("#select-kec").val('default');
        $("#select-kec").selectpicker("refresh");  

    //  #data_kabkot_return = [];
    //  #data_kec_return = [];
    set_kabkot(result.value.split(",")[1]);

    if (select_input.length === 1) {
        // init map provinsi dan auto zoom
        if (select_input[0] === "") {
            if (json_data) {
                json_data.clearLayers();
            }
            lft_map_highlight_kementan.setView([-2.600029, 118.015776], zoom_view);
            arr_fitbound_data = [];
            $("#select-kabkota").val('default');
            $("#select-kabkota").selectpicker("refresh");        
            $("#select-kec").val('default');
            $("#select-kec").selectpicker("refresh");  

            // let detailUrl_poktan = "https://10.1.231.160:8443/public/dashboard/b109e4e9-9085-485c-af30-560e74275837";
            // let detailUrl_prod = "https://10.1.231.160:8443/public/dashboard/c539b776-c2bf-4693-961c-317902aacc30";
            changeIframe();
            // $("#iframe-poktan").attr("src", detailUrl_poktan);
            // $("#iframe-produktivitas").attr("src", detailUrl_prod);
            // alert( arr_fitbound_data.length);
        } else {
            var url = url_server_prov;

            $.ajax({
                url: url,
                dataType: "json",
                success: function (res) {

                    $.each(res.features, function (key, val) {
                        if (val.properties['IDADMIN'].toLowerCase() === select_input[0].toLowerCase()) {
                            // console.log("masuk if");

                            var geojsonFeature = {
                                "type": "Feature",
                                "properties": "",
                                "geometry": {
                                    "type": val.geometry['type'],
                                    "coordinates": val.geometry['coordinates']
                                }
                            };
                            // console.log(geojsonFeature['geometry']);

                            // add the polygon layer to the map
                            if (json_data) {
                                json_data.clearLayers();
                            } // remove highlight sebelumnya

                            if (arr_fitbound_data.length === 0) {
                                arr_fitbound_data.push(geojsonFeature);
                            } else {
                                arr_fitbound_data = [];
                                arr_fitbound_data.push(geojsonFeature);
                            }
                            // alert( arr_fitbound_data.length);
                            json_data.addData(arr_fitbound_data[0]);
                        }
                    })
                    lft_map_highlight_kementan.fitBounds(json_data.getBounds()); // auto zoom ke highlight
                    changeIframe();

                },
                error: function (xhr, status, error) {
                    alert("get_prov ajax gagal");
                    console.log(xhr);
                    console.log(status);
                    console.log(error);
                }
            });
        }
    } else {
        // for development purposes
        alert("Isi arr select_input = " + select_input.length);
    }
}

function set_prov() {
    var url = url_server_prov;
    console.log("set_prov masuk");
    removeOptions("select-prov", "Provinsi"); // remove select option content
    // var form_opt = "<option value='Provinsi'> Provinsi </option>";
    // $("#select-prov").html(form_opt);

    $.ajax({
        url: url,
        dataType: "json",
        success: function (res) {
            console.log("ajax masuk");
            var prov = "";
            form_opt = "";
            // console.log(res);
            $.each(res.features, function (key, val) {
                // console.log(val.properties["ADMIN"]);
                prov = val.properties["ADMIN"];
                form_opt += "<option value='" + prov + "'>" + prov + "</option>";
            })

            $("#select-prov").append(form_opt);

            // for(var i = 0; i < data_prov_server.length; i++){
            //      data_prov_return.push(data_prov_server[i].properties['ADMIN']);                  
            // }
        },
        error: function (xhr, status, error) {
            alert("set_prov ajax gagal");
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
    // console.log(data_prov_server);
    // console.log("masuk 2");

}

function getprov(result) {
    console.log(result);
}

// set kabupaten/kota sebagai isi dari select option kab/kot
function set_kabkot(result) {
    // ----------------------------------------------------------------------------------
    data.provinsi = result
    $.ajax({
        url: "/bigenvelope/api/call/id/1221",
        method: "post",
        data: {
            item: 'provinsi',
            tahun: data.tahun,
            bulan: data.bulan,
            provinsi: data.provinsi
        },
        beforeSend: function () {
            $("#select-kabkota").selectpicker("destroy");
            $("#select-kabkota").html(null);
        },
        success: function (res) {
            if (res.transaction) {
                $("#select-kabkota").append('<option value="">All Kabupaten/Kota</option>');
                $.each(res.data.kabupaten, function (k, v) {
                    $("#select-kabkota").append('<option value="' + v.kode_kab + ',' + v.nm_kab + '">' + v.nm_kab + '</option>');
                });
                $("#select-kabkota").selectpicker();
            }
        }
    });
}

// get inputan dari select option kab/kot
function get_kabkot(result) { // get content from html select kabkot

    if (result.value) {
        var content = result.value.split(",")[0];
        var tmp = 0;
        var tmp_2 = "";
        selected_kab = content;
        selected_kec = ""
        select_input = []
        select_input.push(selected_prov);        
        $("#select-kec").val('default');
        $("#select-kec").selectpicker("refresh");  
        if (select_input.length === 1) {
            select_input.push(content);
        } else if (select_input.length > 1) {
            while (select_input.length > 1) {
                tmp = select_input.pop();
            }
            select_input.push(content);
        }
        var form_opt = "<option value='Kecamatan'> Kecamatan </option>";
        $("#select-kec").html(form_opt);
        // removeOptions($("#select-kec"));
        // console.log(content);

        if (select_input.length === 2) {
            // init map provinsi dan auto zoom
            // console.log("masuk get kabkot");
            var url = url_server_kabkot;

            if (select_input[1] === "Kabupaten/Kota") {
                if (json_data) {
                    json_data.clearLayers(); // remove highlight sebelumnya
                }
                json_data.addData(arr_fitbound_data[0]);
                lft_map_highlight_kementan.fitBounds(json_data.getBounds());
                var form_opt = "<option value='Kecamatan'> Kecamatan </option>";
                $("#select-kec").html(form_opt);
                changeIframe();
                //  removeOptions($("#select-kec"));
            } else {

                $.ajax({
                    url: url,
                    dataType: "json",
                    success: function (res) {

                        $.each(res.features, function (key, val) {
                            if (val.properties['idadmin'].toLowerCase() === select_input[1].toLowerCase()) {
                                // console.log("masuk if");

                                var geojsonFeature = {
                                    "type": "Feature",
                                    "properties": "",
                                    "geometry": {
                                        "type": val.geometry['type'],
                                        "coordinates": val.geometry['coordinates']
                                    }
                                };
                                // console.log(geojsonFeature['geometry']);

                                // add the polygon layer to the map
                                if (json_data) {
                                    json_data.clearLayers();
                                } // remove highlight sebelumnya

                                if (arr_fitbound_data.length > 1) {
                                    while (arr_fitbound_data.length > 1) {
                                        tmp_2 = arr_fitbound_data.pop();
                                        tmp_2 = "";
                                    }
                                }
                                arr_fitbound_data.push(geojsonFeature);
                                // alert( arr_fitbound_data.length);
                                json_data.addData(arr_fitbound_data[1]);
                            }
                        })
                        lft_map_highlight_kementan.fitBounds(json_data.getBounds()); // auto zoom ke highlight
                        changeIframe();
                        set_kecamatan(result.value.split(",")[1]);

                    },
                    error: function (xhr, status, error) {
                        alert("get_kabkot ajax gagal");
                        console.log(xhr);
                        console.log(status);
                        console.log(error);
                    }
                })
                set_kecamatan(result.value.split(",")[1]);
            }
        } else {
            // for development purposes
            alert("Isi arr select_input = " + select_input.length);
        }
    } else {
        result = {};
        result.value = $("#select-prov").val();
        get_prov(result);
        // alert("Error! Isi select_input = " + select_input);
    }
}

// set daftar kecamatan di select option kecamatan
function set_kecamatan(result) {

    if (result) {
        const url = url_server_kecamatan;

        $.ajax({
            url: url,
            dataType: "json",
            success: function (res) {
                data_tmp_kec = [];

                $.each(res.features, function (key, val) {
                    data_tmp_kec.push({
                        idadmin: val.properties.idadmin,
                        geometry: val.geometry
                    });
                })

                // $("#select-kec").html(form_opt);
            },
            error: function (xhr, status, error) {
                alert("set_kecamatan ajax gagal");
                console.log(xhr);
                console.log(status);
                console.log(error);
            }
        });
    } else {
        result = {};
        result.value = $("#select-kabkota").val();
        get_kabkot(result);
        // alert("Error! Isi select_input = " + select_input);
    }


    // -----------------------------------------------------------------------------------------------

    data.kabupaten = result
    $.ajax({
        url: "/bigenvelope/api/call/id/1221",
        method: "post",
        data: {
            item: 'kabupaten',
            tahun: data.tahun,
            bulan: data.bulan,
            provinsi: data.provinsi,
            kabupaten: data.kabupaten
        },
        beforeSend: function () {
            $("#select-kec").selectpicker("destroy");
            $("#select-kec").html(null);
        },
        success: function (res) {
            if (res.transaction) {
                $("#select-kec").append('<option value="">All Kecamatan</option>');
                $.each(res.data.kecamatan, function (k, v) {
                    $("#select-kec").append('<option value="' + v.kode_kec + '">' + v.nm_kec + '</option>');
                });
                $("#select-kec").selectpicker();
            }
        }
    });

}

function selected_tahun(result) {
    select_input = [];
    select_bulan = "";
    data.tahun = result;
    select_tahun = result ? "tahun=" + result : "";
    select_bulan = ""
    selected_prov = ""
    selected_kab = ""
    selected_kec = ""
    $("#select-prov").val('default');
    $("#select-prov").selectpicker("refresh");        
    $("#select-kabkota").val('default');
    $("#select-kabkota").selectpicker("refresh");        
    $("#select-kec").val('default');
    $("#select-kec").selectpicker("refresh");        
    changeIframe();
    $.ajax({
        url: "/bigenvelope/api/call/id/1221",
        method: "post",
        data: {
            item: 'tahun',
            tahun: result
        },
        beforeSend: function () {
            $("#bulan_pil").selectpicker("destroy");
            $("#bulan_pil").html(null);
        },
        success: function (res) {
            if (res.transaction) {
                $("#bulan_pil").append('<option data-hidden="true">Bulan</option>');
                $.each(res.data.bulan, function (k, v) {
                    $("#bulan_pil").append('<option value="' + v.bulan_text + '">' + v.bulan_text + '</option>');
                });
                $("#bulan_pil").selectpicker();
            }
        }
    });

}

function selected_bulan(result) {
    data.bulan = result
    select_bulan = result ? "bulan=" + result : "";
    selected_prov = ""
    selected_kab = ""
    selected_kec = ""  
    $("#select-prov").val('default');
    $("#select-prov").selectpicker("refresh");        
    $("#select-kabkota").val('default');
    $("#select-kabkota").selectpicker("refresh");        
    $("#select-kec").val('default');
    $("#select-kec").selectpicker("refresh");        
    changeIframe();        
    $.ajax({
        url: "/bigenvelope/api/call/id/1221",
        method: "post",
        data: {
            item: 'bulan',
            tahun: data.tahun,
            bulan: data.bulan
        },
        beforeSend: function () {
            $("#select-prov").selectpicker("destroy");
            $("#select-prov").html(null);
        },
        success: function (res) {
            if (res.transaction) {
                $("#select-prov").append('<option value="">All Provinsi</option>');
                $.each(res.data.provinsi, function (k, v) {
                    $("#select-prov").append('<option value="' + v.kode_prov + ',' + v.nm_prov + '">' + v.nm_prov + '</option>');
                });
                $("#select-prov").selectpicker();
            }
        }
    });
}

// get content from html select kecamatan
function get_kecamatan(result) {
    if (result.value) {
        var content = result.value;
        console.log("masuk get_kecamatan");
        selected_kec = content;
        if (select_input.length === 2) {
            select_input.push(content);
        } else if (select_input.length > 2) {
            var tmp = "";
            while (select_input.length > 2) {
                tmp = select_input.pop();
                tmp = "";
            }
            select_input.push(content);
        }

        if (select_input.length === 3) {
            // console.log("masuk get kecamatan");
            // console.log("idx geo kec = "+ idx_geometry_kec);
            // add layer to map kecamatan dan auto zoom
            var kec_split = "";

            if (select_input[2] === "Kecamatan") { // select_input = [provinsi, kabkot, kecamatan]
                if (json_data) {
                    json_data.clearLayers();
                } // remove highlight sebelumnya
                json_data.addData(arr_fitbound_data[1]); // assign highlight kab/kota agar ter highlight di kab/kot
                lft_map_highlight_kementan.fitBounds(json_data.getBounds()); // auto zoom ke kab/kot
                changeIframe();
            } else {
                if (data_tmp_kec.length > 0) {
                    for (let res of data_tmp_kec) {
                        // kec_split = res.properties['ADMIN'].split("-");
                        if (res.idadmin === select_input[2].toLowerCase()) {
                            var geojsonFeature = {
                                "type": "Feature",
                                "properties": "",
                                "geometry": {
                                    "type": res.geometry['type'],
                                    "coordinates": res.geometry['coordinates']
                                }
                            };
                            if (json_data) {
                                json_data.clearLayers();
                            }
                            //  data_fitbound = ;
                            json_data.addData(geojsonFeature);
                        }
                    }
                    lft_map_highlight_kementan.fitBounds(json_data.getBounds());
                    changeIframe();
                } else {
                    // alert("data kecamatan kosong, cek get_kecamatan()")
                }
            }
        } else {
            // for development purposes
            alert("Isi arr select_input = " + select_input);
        }
    } else {
        result = {};
        result.value = $("#select-kabkota").val();
        get_kabkot(result);
        // alert("Error! Isi select_input = " + select_input);
    }
}