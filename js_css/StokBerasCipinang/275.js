const url_iframe_cipinang = "https://10.1.231.160:8443/public/dashboard/0852087b-2ca0-4d98-ab16-c1193242422a";

$(function () {
    $(".m-selectpicker").selectpicker();
    datePicker();
})

function datePicker() {
    var date = {};

    $("#kt_daterangepicker_2").daterangepicker({
        autoApply: true,
        startDate: moment().subtract(30, 'days'),
        endDate: moment()
    },
        function (start, end) {
            date = {
                'start_date': start.format('YYYY-MM-DD'),
                'end_Date': end.format('YYYY-MM-DD')
            }
            $("#txt-date").val(date.start_date + " hingga " + date.end_Date);
            changeIframeDate(date);
            // console.log(date);
        }
    );
    date = {
        'start_date': moment().subtract(30, 'days').format('YYYY-MM-DD'),
        'end_Date': moment().format('YYYY-MM-DD')
    }
    $("#txt-date").val(date.start_date + " hingga " + date.end_Date);
    changeIframeDate(date);
}

function changeIframeDate(date) {
    let query = url_iframe_cipinang + "?tanggal=" + date.start_date + "~" + date.end_Date;
    console.log(query);
    $("#portlet-574").attr("src", query);
}