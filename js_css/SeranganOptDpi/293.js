const url_server_prov = "/assets/kementan/geojson_kementan/DB_PROP.json";
const url_server_kabkot = "/assets/kementan/geojson_kementan/DB_KAB.json";
const src_opt_bun = "https://10.1.231.160:8443/public/dashboard/d82fdea6-9f02-486c-93a2-6735b7541dea";
const src_opt_tp = "https://10.1.231.160:8443/public/dashboard/fecf160a-b731-46f1-b05e-23544a2497c9";
const src_dpi_tp = "https://10.1.231.160:8443/public/dashboard/8749f3cb-6536-4958-87d6-6caf1109ee1e";
var zoom_view = 5;
var lft_map_opt; // simpan openstreetmap tile untuk opt
var json_data_opt; // layer highlight untuk leaflet
var select_input = [];
var polygon_konsumen = [];
var jenis_iframe = "opt-bun";

$(function () {
    initMap();
    $(".m-selectpicker").selectpicker();

    $(".bulan-pil").css("display", "none");

    $("#opt_dpi_pil").on('change', changeOptDpi);
    $("#provinsi_pil").on("change", changeIframeProvinsi);
    $("#tahun_pil").on("change", changeIframeTahun);
    $("#bulan_pil").on("change", changeIframeBulan);
    $('#kabkot_pil').on("change", changeIframeKab);
    $("#periode_pil").on("change", changeIframePeriode);
})

function getBulan() {
    $.ajax({
        url: "/bigenvelope/api/call/id/2595",
        method: "post",
        beforeSend: function () {
            $("#bulan_pil").selectpicker("destroy");
            $("#bulan_pil").html(null);
        },
        success: function (res) {
            // console.log(res);
            if (res.transaction) {
                $("#bulan_pil").append('<option value="">All Bulan</option>');
                $.each(res.data.bulan, function (k, v) {
                    console.log(v);
                    $("#bulan_pil").append('<option value="' + v.bulan_teks + '">' + v.bulan_teks + '</option>');
                });
                $("#bulan_pil").selectpicker();
            }
        }
    });
}

function removeElement(limit, arr) {
    while (arr.length > limit) {
        arr.pop();
    }
}

function initMap() {
    const default_coord1 = ["Center", -2.600029, 118.015776];
    // leaflet init: id_html -> unntuk selector id tag di html
    // set_view -> untuk assign koordinat default ketika map di-load
    // zoom -> untuk set zoom level default ketika map di-load
    lft_map_opt = L.map('map-highlight-opt', {
        minZoom: 4
    }).setView([default_coord1[1], default_coord1[2]], zoom_view);

    // OpenStreetMap init
    const OpenStreetMapUrl = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";

    const OpenStreetMapTileOPT = L.tileLayer(OpenStreetMapUrl, {
        minZoom: 1,
        maxZoom: 20,
        attribution: "<a href='http://www.openstreetmap.org'> &copy;OpenStreetMap </a>, <a href='http://www.pertanian.go.id'> Kementan </a>"
    });

    OpenStreetMapTileOPT.addTo(lft_map_opt);

    // tambah layer untuk highlight
    json_data_opt = L.geoJSON().addTo(lft_map_opt);
}

function changeOptDpi() {
    $('#tahun').val('').selectpicker('refresh');
    $("#provinsi_pil").val('').selectpicker('refresh');
    $("#tahun_pil").val('').selectpicker('refresh');
    $("#bulan_pil").val('').selectpicker('refresh');
    $('#kabkot_pil').val('').selectpicker('refresh');
    $("#periode_pil").val('').selectpicker('refresh');
    changeIframeProvinsi();
    let value = $("#opt_dpi_pil").val();

    jenis_iframe = value;
    select_input = [];
    if (value == "opt-bun") {
        $("#map-title").html("MAP Organisme Pengganggu Tanaman untuk Komoditas Perkebunan");
        $(".bulan-pil").hide();
        $(".kabkot-pil").show();
        $(".periode-pil").show();
        $("#portlet-opt").attr("src", src_opt_bun);
    } else if (value == "opt-tp") {
        // getBulan();
        $("#map-title").html("MAP Organisme Pengganggu Tanaman untuk Komoditas Tanaman Pangan");
        $(".bulan-pil").show();
        $(".kabkot-pil").hide();
        $(".periode-pil").hide();
        $("#portlet-opt").attr("src", src_opt_tp);
    } else if (value == "dpi-tp") {
        // getBulan();
        $("#map-title").html("MAP Dampak Perubahan Iklim untuk Komoditas Tanaman Pangan");
        $(".bulan-pil").show();
        $(".kabkot-pil").hide();
        $(".periode-pil").hide();
        $("#portlet-opt").attr("src", src_dpi_tp);
    }
}

function evaluateMapKonsumen(geoData, search_val, administratif) {
    if (polygon_konsumen.length == 0) {
        polygon_konsumen.push(geoData);
        json_data_opt.clearLayers();
        json_data_opt.addData(geoData);
        lft_map_opt.fitBounds(json_data_opt.getBounds());
    } else if (polygon_konsumen.length == 1) {
        if (search_val == "") {
            polygon_konsumen.splice(0, 1);
            json_data_opt.clearLayers();
            lft_map_opt.fitBounds(json_data_opt.getBounds());
        } else {
            if (polygon_konsumen[0].properties.administrative === administratif) {
                polygon_konsumen[0] = geoData;
                json_data_opt.clearLayers();
                json_data_opt.addData(polygon_konsumen[0]);
                lft_map_opt.fitBounds(json_data_opt.getBounds());
            } else {
                polygon_konsumen.push(geoData);
                if (polygon_konsumen[0].properties.administrative == "kab/kota" &&
                    polygon_konsumen[1].properties.administrative == "provinsi") {
                    json_data_opt.clearLayers();
                    json_data_opt.addData(polygon_konsumen[0]);
                    lft_map_opt.fitBounds(json_data_opt.getBounds());
                } else if (polygon_konsumen[1].properties.administrative == "kab/kota" &&
                    polygon_konsumen[0].properties.administrative == "provinsi") {
                    json_data_opt.clearLayers();
                    json_data_opt.addData(polygon_konsumen[1]);
                    lft_map_opt.fitBounds(json_data_opt.getBounds());
                }
            }
        }
    } else if (polygon_konsumen.length == 2) {
        if (search_val == "") {
            if (polygon_konsumen[0].properties.administrative === administratif) {
                polygon_konsumen.splice(0, 1);
            } else if (polygon_konsumen[1].properties.administrative === administratif) {
                polygon_konsumen.splice(1, 1);
            }
            json_data_opt.clearLayers();
            json_data_opt.addData(polygon_konsumen[0]);
            lft_map_opt.fitBounds(json_data_opt.getBounds());
        } else {
            if (polygon_konsumen[0].properties.administrative === administratif) {
                polygon_konsumen[0] = geoData;
            } else if (polygon_konsumen[1].properties.administrative === administratif) {
                polygon_konsumen[1] = geoData;
            }

            if (administratif === "provinsi") {
                if (polygon_konsumen[0].properties.administrative == "kab/kota") {
                    polygon_konsumen.splice(0, 1);
                } else if (polygon_konsumen[1].properties.administrative == "kab/kota") {
                    polygon_konsumen.splice(1, 1);
                }
                json_data_opt.clearLayers();
                json_data_opt.addData(polygon_konsumen);
                lft_map_opt.fitBounds(json_data_opt.getBounds());
            } else if (administratif === "kab/kota") {
                if (polygon_konsumen[0].properties.administrative == "kab/kota" &&
                    polygon_konsumen[1].properties.administrative == "provinsi") {
                    json_data_opt.clearLayers();
                    json_data_opt.addData(polygon_konsumen[0]);
                    lft_map_opt.fitBounds(json_data_opt.getBounds());
                } else if (polygon_konsumen[1].properties.administrative == "kab/kota" &&
                    polygon_konsumen[0].properties.administrative == "provinsi") {
                    json_data_opt.clearLayers();
                    json_data_opt.addData(polygon_konsumen[1]);
                    lft_map_opt.fitBounds(json_data_opt.getBounds());
                }
            }
        }
    }
}

function searchGeoJsonKonsumen(url, administratif, kodeDaerah) {
    $.ajax({
        url: url,
        dataType: "json",
        success: function (res) {
            console.log("ajax masuk", res);
            var mark = 0;
            var geo_data = {};
            $.each(res.features, function (key, val) {
                if (administratif === "provinsi") {
                    if (val.properties['IDADMIN'] == kodeDaerah) {
                        console.log("masuk search prov");
                        mark = 1;
                        geo_data = {
                            "type": "Feature",
                            "properties": {
                                "administrative": administratif
                            },
                            "geometry": {
                                "type": val.geometry['type'],
                                "coordinates": val.geometry['coordinates']
                            }
                        };
                    }
                } else if (administratif === "kab/kota") {
                    if (val.properties['idadmin'] == kodeDaerah) {
                        console.log("masuk search kabkot");
                        mark = 1;
                        geo_data = {
                            "type": "Feature",
                            "properties": {
                                "administrative": administratif
                            },
                            "geometry": {
                                "type": val.geometry['type'],
                                "coordinates": val.geometry['coordinates']
                            }
                        };
                    }
                }
            })
            if (mark === 0) {
                toastr["error"]("Geometry map data not found", "Alert!!");

                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            } else {
                if (json_data_opt && geo_data) {
                    evaluateMapKonsumen(geo_data, kodeDaerah, administratif);
                }
            }
        },
        error: function (xhr, status, error) {
            alert("search geojson ajax gagal");
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
}

function highlightMap(kodeDaerahDB, administratif) {
    if (kodeDaerahDB != "") {
        if (administratif == "provinsi") {
            searchGeoJsonKonsumen(url_server_prov, "provinsi", kodeDaerahDB);
        } else if (administratif == "kab/kota") {
            searchGeoJsonKonsumen(url_server_kabkot, "kab/kota", kodeDaerahDB);
        }
    } else {
        json_data_opt ? json_data_opt.clearLayers() : '';
        if (administratif == "provinsi") {
            polygon_konsumen = [];
            lft_map_opt.setView([-2.600029, 118.015776], zoom_view);
        } else if (polygon_konsumen.length == 2) {
            if (administratif == polygon_konsumen[0].properties.administrative) {
                polygon_konsumen.splice(0, 1);
            } else if (administratif == polygon_konsumen[1].properties.administrative) {
                polygon_konsumen.splice(1, 1);
            }
            json_data_opt.addData(polygon_konsumen);
            lft_map_opt.fitBounds(json_data_opt.getBounds());
        } else if (polygon_konsumen.length == 1) {
            polygon_konsumen.splice(0, 1);
            lft_map_opt.setView([-2.600029, 118.015776], zoom_view);
        }
    }
}

function removeElement(limit, arr) {
    while (arr.length > limit) {
        arr.pop();
    }
}

function changeIframeTemplate(select_id_val, iframe_url_selector, filter_type) {
    let url_selector_idx0 = "?" + iframe_url_selector + "=";
    let url_selector_idxN = "&" + iframe_url_selector + "=";
    if (filter_type == 'tahun') {
        select_input.map((val, index) => {
            if (val.includes('provinsi')) {
                removeElement(index, select_input);
            } else if (val.includes('bulan')) {
                removeElement(index, select_input);
            } else if (val.includes('kabupaten_kota')) {
                removeElement(index, select_input);
            }
        })
    } else if (filter_type == 'bulan') {
        select_input.map((val, index) => {
            if (val.includes('provinsi')) {
                removeElement(index, select_input);
            } else if (val.includes('kabupaten_kota')) {
                removeElement(index, select_input);
            }
        })
    } else if (filter_type == 'provinsi') {
        select_input.map((val, index) => {
            if (val.includes('kabupaten_kota')) {
                removeElement(index, select_input);
            }
        })
    }

    if (select_input.length == 0) {
        let tmp = select_id_val ? url_selector_idx0 + select_id_val : '';
        select_input.push(tmp);
    } else if (select_input.length == 1) {
        if (select_input[0].includes(url_selector_idx0)) {
            select_input[0] = url_selector_idx0 + select_id_val;
            select_id_val ? '' : select_input.splice(0, 1);
        } else if (filter_type == "provinsi") {
            if (select_input[0].includes("?tahun") || select_input[0].includes("?kabupaten_kota") ||
                select_input[0].includes("?bulan") || select_input[0].includes("?periode")) {
                removeElement(1, select_input);
                select_input.push(url_selector_idxN + select_id_val);
            }
        } else if (filter_type == "kab/kota") {
            if (select_input[0].includes("?tahun") || select_input[0].includes("?provinsi") ||
                select_input[0].includes("?bulan") || select_input[0].includes("?periode")) {
                removeElement(1, select_input);
                select_input.push(url_selector_idxN + select_id_val);
            }
        } else if (filter_type == "tahun") {
            if (select_input[0].includes("?bulan") || select_input[0].includes("?provinsi") ||
                select_input[0].includes("?kabupaten_kota") || select_input[0].includes("?periode")) {
                removeElement(1, select_input);
                select_input.push(url_selector_idxN + select_id_val);
            }
        } else if (filter_type == "bulan") {
            if (select_input[0].includes("?tahun") || select_input[0].includes("?provinsi") ||
                select_input[0].includes("?kabupaten_kota") || select_input[0].includes("?periode")) {
                removeElement(1, select_input);
                select_input.push(url_selector_idxN + select_id_val);
            }
        } else if (filter_type == "periode") {
            if (select_input[0].includes("?tahun") || select_input[0].includes("?provinsi") ||
                select_input[0].includes("?kabupaten_kota") || select_input[0].includes("?bulan")) {
                removeElement(1, select_input);
                select_input.push(url_selector_idxN + select_id_val);
            }
        } else {
            console.log("Error di select input length == 1 => " + select_input)
        }
    } else if (select_input.length == 2) {
        if (select_input[0].includes("?") && select_input[1].includes("&")) {
            if ((!select_input[0].includes(iframe_url_selector) && !select_input[1].includes(iframe_url_selector))) {
                removeElement(2, select_input);
                select_input.push(url_selector_idxN + select_id_val);
            } else if (select_input[0].includes(url_selector_idx0)) {
                select_input[0] = url_selector_idx0 + select_id_val;
                select_id_val ? '' : select_input.splice(0, 1);
                select_input[0] = select_input[0].replace(/[&]/g, "?");
            } else if (select_input[1].includes(url_selector_idxN)) {
                select_input[1] = url_selector_idxN + select_id_val;
                select_id_val ? '' : select_input.splice(1, 1);
            }
        }
        // alert("arr select_input = " + select_input);
    } else if (select_input.length == 3) {
        if (!select_input[0].includes(url_selector_idx0) && !select_input[1].includes(url_selector_idxN) &&
            !select_input[2].includes(url_selector_idxN)) {

            removeElement(3, select_input);
            select_input.push(url_selector_idxN + select_id_val);

        } else if (select_input[0].includes(url_selector_idx0) && !select_input[1].includes(url_selector_idxN) &&
            !select_input[2].includes(url_selector_idxN)) {

            removeElement(3, select_input);
            select_input[0] = url_selector_idx0 + select_id_val;
            select_id_val ? '' : select_input.splice(0, 1);
            select_input[0] = select_input[0].replace(/[&]/g, "?");

        } else if (!select_input[0].includes(url_selector_idx0) && select_input[1].includes(url_selector_idxN) &&
            !select_input[2].includes(url_selector_idxN)) {

            removeElement(3, select_input);
            select_input[1] = url_selector_idxN + select_id_val;
            select_id_val ? '' : select_input.splice(1, 1);

        } else if (!select_input[0].includes(url_selector_idx0) && !select_input[1].includes(url_selector_idxN) &&
            select_input[2].includes(url_selector_idxN)) {

            removeElement(3, select_input);
            select_input[2] = url_selector_idxN + select_id_val;
            select_id_val ? '' : select_input.splice(2, 1);
        }
    } else if (select_input.length == 4) {
        if (select_input[0].includes(url_selector_idx0) && !select_input[1].includes(url_selector_idxN) &&
            !select_input[2].includes(url_selector_idxN) && !select_input[3].includes(url_selector_idxN)) {

            removeElement(4, select_input);
            select_input[0] = url_selector_idx0 + select_id_val;
            select_id_val ? '' : select_input.splice(0, 1);
            select_input[0] = select_input[0].replace(/[&]/g, "?");

        } else if (!select_input[0].includes(url_selector_idx0) && select_input[1].includes(url_selector_idxN) &&
            !select_input[2].includes(url_selector_idxN) && !select_input[3].includes(url_selector_idxN)) {

            removeElement(4, select_input);
            select_input[1] = url_selector_idxN + select_id_val;
            select_id_val ? '' : select_input.splice(1, 1);

        } else if (!select_input[0].includes(url_selector_idx0) && !select_input[1].includes(url_selector_idxN) &&
            select_input[2].includes(url_selector_idxN) && !select_input[3].includes(url_selector_idxN)) {

            removeElement(4, select_input);
            select_input[2] = url_selector_idxN + select_id_val;
            select_id_val ? '' : select_input.splice(2, 1);

        } else if (!select_input[0].includes(url_selector_idx0) && !select_input[1].includes(url_selector_idxN) &&
            !select_input[2].includes(url_selector_idxN) && select_input[3].includes(url_selector_idxN)) {

            removeElement(4, select_input);
            select_input[3] = url_selector_idxN + select_id_val;
            select_id_val ? '' : select_input.splice(3, 1);

        }
    }

    var new_src_iframe;
    if (jenis_iframe == "opt-bun") {
        new_src_iframe = src_opt_bun + select_input.join("");
    } else if (jenis_iframe == "opt-tp") {
        new_src_iframe = src_opt_tp + select_input.join("");
    } else if (jenis_iframe == "dpi-tp") {
        new_src_iframe = src_dpi_tp + select_input.join("");
    }
    console.log(new_src_iframe);
    $("#portlet-opt").attr("src", new_src_iframe);
}

function changeIframeBulan() {
    // console.log(date); 
    // getProvinsi();
    // getKabupaten();
    let bulan_pil = $("#bulan_pil").val();
    changeIframeTemplate(bulan_pil, 'bulan', 'bulan');

}

function changeIframeTahun() {
    // console.log(date);
    // getBulan();
    // getProvinsi();
    // getKabupaten();
    let tahun_select = $("#tahun_pil").val();
    changeIframeTemplate(tahun_select, 'tahun', 'tahun');

}

function changeIframePeriode() {
    // console.log(date);
    // getBulan();
    // getProvinsi();
    // getKabupaten();
    let periode_select = $("#periode_pil").val();
    changeIframeTemplate(periode_select, 'periode', 'periode');

}

function changeIframeProvinsi() {
    // console.log(date);
    // getKabupaten();
    $.ajax({
        url: "/bigenvelope/api/call/id/2595",
        method: "post",
        data: {
            item: 'kabupaten',
            provinsi: $("#provinsi_pil").val().split(",")[1]
        },
        beforeSend: function () {
            $("#kabkot_pil").selectpicker("destroy");
            $("#kabkot_pil").html(null);
        },
        success: function (res) {
            if (res.transaction) {
                $("#kabkot_pil").append('<option value="">All Kabupaten/Kota</option>');
                $.each(res.data.kabupaten, function (k, v) {
                    $("#kabkot_pil").append('<option value="' + v.kode_kab + ',' + v.nama_kab + '">' + v.nama_kab + '</option>');
                });
                $("#kabkot_pil").selectpicker();
            }
        }
    });
    var prov_select;
    if ($("#provinsi_pil").val() == "") {
        highlightMap('', 'provinsi');
    } else {
        let tmp = $("#provinsi_pil").val().split(",");
        prov_select = tmp[1].split(" ").join("%20");
        highlightMap(tmp[0].split(" ")[0], 'provinsi');
    }

    changeIframeTemplate(prov_select, 'provinsi', 'provinsi');
}

function changeIframeKab() {
    // console.log(date);
    var kab_select;
    console.log($("#kabkot_pil").val());
    if ($("#kabkot_pil").val() == "") {
        highlightMap('', 'kab/kota');
    } else {
        let tmp = $("#kabkot_pil").val().split(",");
        console.log("tmp = ", tmp);
        kab_select = tmp[1].split(" ").join("%20");
        console.log("iframe kab = ", kab_select);
        highlightMap(tmp[0].split(" ")[0], 'kab/kota');
    }
    changeIframeTemplate(kab_select, 'kabupaten_kota', 'kab/kota');
}