let src_portlet_650 = 'https://10.1.231.160:8443/public/dashboard/3632db71-a509-48b6-b35e-a66e1fa50dd5';

$(function () {
    datePicker();
})

function datePicker() {
    var date = {};
    $("#kt_daterangepicker_2").daterangepicker({
            autoApply: true,
            startDate: moment().subtract(60, 'days'),
            maxDate: moment()
        },
        function (start, end) {
            date = {
                'start_date': start.format('YYYY-MM-DD'),
                'end_Date': end.format('YYYY-MM-DD')
            }
            $("#txt-date").val(date.start_date + " hingga " + date.end_Date);
            changeIframe(date);
            // console.log(date);
        }
    );
    date = {
        'start_date': moment().subtract(60, 'days').format('YYYY-MM-DD'),
        'end_Date': moment().format('YYYY-MM-DD')
    }
    $("#txt-date").val(date.start_date + " hingga " + date.end_Date);
    changeIframe(date);
}

function changeIframe(date) {
    // console.log(src_portlet_650 + "?tanggal=" + date.start_date + "~" + date.end_Date);
    let new_src_portlet_650 = src_portlet_650 + "?tanggal=" + date.start_date + "~" + date.end_Date;
    $("#portlet-573").attr("src", new_src_portlet_650);
}