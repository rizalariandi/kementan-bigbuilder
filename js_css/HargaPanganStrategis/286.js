let src_portlet_653 = "https://10.1.231.160:8443/public/dashboard/94574d55-b67b-4690-9d8b-24427ae8e15e"; // portlet nasional produsen
let src_produsen_provinsi = "https://10.1.231.160:8443/public/dashboard/962c1eb8-d5c6-4fa7-a726-c74822cde07a";
let src_konsumen_nasional = "https://10.1.231.160:8443/public/dashboard/6b0de211-2604-4cd4-a705-f4b84b53c784"; // portlet nasional konsumen
let src_konsumen_provinsi = "https://10.1.231.160:8443/public/dashboard/49462c34-ecae-4b45-8ee3-e74c68abd9a1"; // provinsi iframe konsumen
let src_konsumen_kabupaten = "https://10.1.231.160:8443/public/dashboard/f1032225-e0a4-499c-85fc-cb92154c74b9"; // kab iframe konsumen
var select_input = [];


$(function () {
	$(".m-selectpicker").selectpicker();

	// let tmp_tgl = getCurrentDate();
	// select_input.push("?tanggal=" + tmp_tgl);
	// console.log("tgl = " + select_input);

	$("#provinsi_1").on("change", changeProvinsiProdusen);
	$("#provinsi_2").on("change", changeProvinsiKonsumen);
	// $("#kabupaten_1").on("change", changeKabupaten);

});

function deleteSelectInput(limit) {
	while (select_input.length > limit) {
		select_input.pop();
	}
}

function getCurrentDate() {
	var month = new Array();
	month[0] = "01"; // january
	month[1] = "02"; // february
	month[2] = "03"; // march
	month[3] = "04"; // april
	month[4] = "05"; // may
	month[5] = "06"; // june
	month[6] = "07"; // july
	month[7] = "08"; // august
	month[8] = "09"; // september
	month[9] = "10"; // october
	month[10] = "11"; // november
	month[11] = "12"; // december

	var date = new Date();
	console.log(date.getFullYear() + "-" + month[date.getMonth()] + "-" + date.getDate());
	return date.getFullYear() + "-" + month[date.getMonth()] + "-" + date.getDate();
}

function changeProvinsiProdusen() {
	let prov_select = $("#provinsi_1").val();
    $.ajax({
		url: "/bigenvelope/api/call/id/329",
		method: "post",
		data: {
            item: $("#provinsi_1").val() ? 'provinsi' : '',
            provinsi:$("#provinsi_1").val()
		},
		beforeSend: function () {			
		},
		success: function (res) {
			if (res.transaction) {
				$("#tanggal_produsen").text(res.data.tanggal);
			}
        }
    });
	if (prov_select === "") {
		$("#portlet-1018").attr("src", src_portlet_653);
	} else {
		let new_src_653 = src_produsen_provinsi + "?provinsi=" + prov_select;
		$("#portlet-1018").attr("src", new_src_653);
	}
}

function removeElement(limit, arr) {
	while (arr.length > limit) {
		arr.pop();
	}
}
// function changeProvinsi() {
// 	getKabupaten();
// 	getKecamatan();

// 	// console.log("isi prov_2 => ", $("#provinsi_2").val());
// 	let prov_select = $("#provinsi_2").val();

// 	if (prov_select === "") {
// 		let src_633 = "http://172.17.62.16:3000/public/dashboard/69c6837a-fee1-4b38-96bc-3f7176d1a324";
// 		$("#portlet-1017").attr("src", src_633);
// 		select_input = [];
// 	} else {
// 		if (select_input.length == 0) {
// 			select_input.push(prov_select);
// 		} else {
// 			select_input = [];
// 			select_input.push(prov_select);
// 		}
// 		let src_648 = portlet_648 + "?provinsi=" + select_input[0];
// 		$("#portlet-1017").attr("src", src_648);
// 	}
// }

// function changeKabupaten() {
// 	getKecamatan();

// 	let kab_select = $("#kabupaten_1").val();

// 	if (kab_select === "") {
// 		let src_648 = portlet_648 + "?provinsi=" + select_input[0];
// 		$("#portlet-1017").attr("src", src_648);
// 		deleteSelectInput(1);
// 	} else {
// 		if (select_input.length == 1) {
// 			select_input.push(kab_select);
// 		} else {
// 			deleteSelectInput(1);
// 			select_input.push(kab_select);
// 		}
// 		let src_kab_konsumen = iframe_kab_konsumen + "?provinsi=" + select_input[0] + "&regencies_nm=" + select_input[1]; // sementara sebelum dibuat model
// 		$("#portlet-1017").attr("src", src_kab_konsumen);
// 	}

// }

function changeIframeTemplate(select_id_val, iframe_url_selector, filter_type) {
	let url_selector_idx0 = "?" + iframe_url_selector + "=";
	let url_selector_idxN = "&" + iframe_url_selector + "=";
	select_input = filter_type == "provinsi" ? [] : select_input;
	if (select_input.length == 0) {
		let tmp = select_id_val ? url_selector_idx0 + select_id_val : '';
		select_input.push(tmp);
		if (filter_type == "provinsi") {
			let new_src_iframe = src_konsumen_provinsi + select_input.join("");
			console.log(new_src_iframe);
			$("#portlet-1017").attr("src", new_src_iframe);
		} else if (filter_type == "kab/kota") {
			let new_src_iframe = src_konsumen_kabupaten + select_input.join("");
			console.log(new_src_iframe);
			$("#portlet-1017").attr("src", new_src_iframe);
		}
	} else if (select_input.length == 1) {
		if (select_input[0].includes(url_selector_idx0)) {
			select_input[0] = url_selector_idx0 + select_id_val;
			if (select_id_val == '') {
				select_input.splice(0, 1);
				$("#portlet-1017").attr("src", src_konsumen_nasional);
				return;
			}
		} else if (filter_type == "provinsi" && select_input[0].includes("?kabupaten_kota")) {
			removeElement(1, select_input);
			select_input.push(url_selector_idxN + select_id_val);
		} else if (filter_type == "kab/kota" && select_input[0].includes("?provinsi")) {
			removeElement(1, select_input);
			select_input.push(url_selector_idxN + select_id_val);
		} else {
			console.log("Error di select input length == 1 => " + select_input)
		}
		let new_src_iframe = src_konsumen_kabupaten + select_input.join("");
		console.log(new_src_iframe);
		$("#portlet-1017").attr("src", new_src_iframe);
	} else if (select_input.length == 2) {
		if (select_id_val == '') {
			if (filter_type == 'provinsi') {
				select_input.splice(0, 2);
				let new_src_iframe = src_konsumen_kabupaten + select_input.join("");
				console.log(new_src_iframe);
				$("#portlet-1017").attr("src", new_src_iframe);
			} else if (filter_type == 'kab/kota') {
				if (select_input[0].includes("kabupaten_kota")) {
					select_input.splice(0, 1);
					select_input[0] = select_input[0].replace(/[&]/g, "?");
				} else if (select_input[1].includes("kabupaten_kota")) {
					select_input.splice(1, 1);
				}
				let new_src_iframe = src_konsumen_provinsi + select_input.join("");
				console.log(new_src_iframe);
				$("#portlet-1017").attr("src", new_src_iframe);
			}
		} else {
			if (select_input[0].includes("?kabupaten_kota") && select_input[1].includes("&provinsi")) {
				if (filter_type == "provinsi") {
					select_input[1] = url_selector_idxN + select_id_val;
				} else if (filter_type == "kab/kota") {
					select_input[0] = url_selector_idx0 + select_id_val;
				}
			} else if (select_input[0].includes("?provinsi") && select_input[1].includes("&kabupaten_kota")) {
				if (filter_type == "provinsi") {
					select_input[0] = url_selector_idx0 + select_id_val;
				} else if (filter_type == "kab/kota") {
					select_input[1] = url_selector_idxN + select_id_val;
				}
			}
			let new_src_iframe = src_konsumen_kabupaten + select_input.join("");
			console.log(new_src_iframe);
			$("#portlet-1017").attr("src", new_src_iframe);
		}
		// alert("arr select_input = " + select_input);
	}

//	let new_src_iframe_pihc = src_iframe_pihc + select_input.join("");
//	console.log(new_src_iframe_pihc);
//	$("#portlet-690").attr("src", new_src_iframe_pihc);
	// return select_input.join("");
}

function changeProvinsiKonsumen() {
    getKabupaten();
    $.ajax({
		url: "/bigenvelope/api/call/id/330",
		method: "post",
		data: {
            item: $("#provinsi_2").val() ? 'provinsi' : '',
            provinsi: $("#provinsi_2").val()
		},
		beforeSend: function () {			
		},
		success: function (res) {
			if (res.transaction) {
				$("#tanggal_konsumen").text(res.data.tanggal);
			}
        }
    });

	let tmp = $("#provinsi_2").val().split(" ");
	let prov_select = tmp.join("%20");
	changeIframeTemplate(prov_select, 'provinsi', 'provinsi');
}

function changeKabupaten(kab_select) {
	// let tmp = $("#kabupaten_1").val().split(" ");
    // let kab_select = tmp.join("%20");    
    $.ajax({
		url: "/bigenvelope/api/call/id/330",
		method: "post",
		data: {
            item: kab_select ? 'kabupaten': ($("#provinsi_2").val() ? 'provinsi' : ''),
            provinsi: $("#provinsi_2").val(),
            kabupaten: kab_select
		},
		beforeSend: function () {			
		},
		success: function (res) {
			if (res.transaction) {
				$("#tanggal_konsumen").text(res.data.tanggal);
			}
		}
	});
	changeIframeTemplate(kab_select, 'kabupaten_kota', 'kab/kota');
}

function getKabupaten() {

	$.ajax({
		url: "/bigenvelope/api/call/id/2589",
		method: "post",
		data: {
			kd_prov: $("#provinsi_2").val()
		},
		beforeSend: function () {
			$("#kabupaten_1").selectpicker("destroy");
			$("#kabupaten_1").html(null);
		},
		success: function (res) {
			if (res.transaction) {
				$("#kabupaten_1").append('<option value="">All Kabupaten/Kota</option>');
				$.each(res.data, function (k, v) {
					$("#kabupaten_1").append('<option value="' + v.nm_kab + '">' + v.nm_kab + '</option>');
				});
				$("#kabupaten_1").selectpicker();
			}
		}
	});

}