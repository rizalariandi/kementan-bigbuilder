let src_iframe_benih = "https://10.1.231.160:8443/public/dashboard/d7af6a42-6724-42f7-a22f-d785fd9772a7";
var select_input = []

$(function () {
    $(".m-selectpicker").selectpicker();

    $("#provinsi_pil").on("change", changeIframeProvinsi);
    $("#tahun_pil").on("change", changeIframeTahun);
    $("#bulan_pil").on("change", changeIframeBulan);
})

function removeElement(limit, arr) {
    while (arr.length > limit) {
        arr.pop();
    }
}

function changeIframeBulan() {
    // console.log(date);
    select_input.map((val, index) => {
        if (val.includes('provinsi')) {
            removeElement(index, select_input);
        }
    })
    getProvinsi()
    let bulan_pil = $("#bulan_pil").val();
    if (select_input.length == 0) {
        let tmp = bulan_pil ? "?bulan=" + bulan_pil : '';
        select_input.push(tmp);
    } else if (select_input.length == 1) {
        if (select_input[0].includes("?bulan")) {
            select_input[0] = "?bulan=" + bulan_pil;
            bulan_pil ? '' : select_input.splice(0, 1);
        } else if (select_input[0].includes("?tahun") || select_input[0].includes("?provinsi")) {
            removeElement(1, select_input);
            select_input.push("&bulan=" + bulan_pil);
        } else {
            console.log("Error di changeiframedate= " + select_input)
        }
    } else if (select_input.length == 2) {
        if ((select_input[0].includes("?provinsi") && select_input[1].includes("&tahun")) ||
            (select_input[0].includes("?tahun") && select_input[1].includes("&provinsi"))) {
            removeElement(2, select_input);
            select_input.push("&bulan=" + bulan_pil);
        } else if (select_input[0].includes("?bulan")) {
            select_input[0] = "?bulan=" + bulan_pil;
            bulan_pil ? '' : select_input.splice(0, 1);
            select_input[0] = select_input[0].replace(/[&]/g, "?");
        } else if (select_input[1].includes("&bulan")) {
            select_input[1] = "&bulan=" + bulan_pil;
            bulan_pil ? '' : select_input.splice(1, 1);
        }
        // alert("arr select_input = " + select_input);
    } else if (select_input.length == 3) {
        if (select_input[0].includes("?bulan") && !select_input[1].includes("&bulan") && !select_input[2].includes("&bulan")) {
            removeElement(3, select_input);
            select_input[0] = "?bulan=" + bulan_pil;
            bulan_pil ? '' : select_input.splice(0, 1);
            select_input[0] = select_input[0].replace(/[&]/g, "?");
        } else if (!select_input[0].includes("?bulan") && select_input[1].includes("&bulan") && !select_input[2].includes("&bulan")) {
            removeElement(3, select_input);
            select_input[1] = "&bulan=" + bulan_pil;
            bulan_pil ? '' : select_input.splice(1, 1);
        } else if (!select_input[0].includes("?bulan") && !select_input[1].includes("&bulan") && select_input[2].includes("&bulan")) {
            removeElement(3, select_input);
            select_input[2] = "&bulan=" + bulan_pil;
            bulan_pil ? '' : select_input.splice(2, 1);
        }
    }

    // let new_src_iframe_benih = src_iframe_benih + "?bulan=" + date.start_date + "~" + date.end_Date;
    let tmp_kel = src_iframe_benih + select_input.join("");
    console.log(tmp_kel);
    $("#iframe-329").attr("src", tmp_kel);
}

function changeIframeProvinsi() {
    // console.log(date);
    let tmp = $("#provinsi_pil").val().split(" ");
    let prov_select = tmp.join("%20");

    if (select_input.length == 0) {
        let tmp = prov_select ? "?provinsi=" + prov_select : '';
        select_input.push(tmp);
    } else if (select_input.length == 1) {
        if (select_input[0].includes("?provinsi")) {
            select_input[0] = "?provinsi=" + prov_select;
            prov_select ? '' : select_input.splice(0, 1);
        } else if (select_input[0].includes("?tahun") || select_input[0].includes("?bulan")) {
            removeElement(1, select_input);
            select_input.push("&provinsi=" + prov_select);
        } else {
            console.log("Error di changeiframedate= " + select_input)
        }
    } else if (select_input.length == 2) {
        if ((select_input[0].includes("?bulan") && select_input[1].includes("&tahun")) ||
            (select_input[0].includes("?tahun") && select_input[1].includes("&bulan"))) {
            removeElement(2, select_input);
            select_input.push("&provinsi=" + prov_select);
        } else if (select_input[0].includes("?provinsi")) {
            select_input[0] = "?provinsi=" + prov_select;
            prov_select ? '' : select_input.splice(0, 1);
            select_input[0] = select_input[0].replace(/[&]/g, "?");
        } else if (select_input[1].includes("&provinsi")) {
            select_input[1] = "&provinsi=" + prov_select;
            prov_select ? '' : select_input.splice(1, 1);
        }
        // alert("arr select_input = " + select_input);
    } else if (select_input.length == 3) {
        if (select_input[0].includes("?provinsi") && !select_input[1].includes("&provinsi") && !select_input[2].includes("&provinsi")) {
            removeElement(3, select_input);
            select_input[0] = "?provinsi=" + prov_select;
            prov_select ? '' : select_input.splice(0, 1);
            select_input[0] = select_input[0].replace(/[&]/g, "?");
        } else if (!select_input[0].includes("?provinsi") && select_input[1].includes("&provinsi") && !select_input[2].includes("&provinsi")) {
            removeElement(3, select_input);
            select_input[1] = "&provinsi=" + prov_select;
            prov_select ? '' : select_input.splice(1, 1);
        } else if (!select_input[0].includes("?provinsi") && !select_input[1].includes("&provinsi") && select_input[2].includes("&provinsi")) {
            removeElement(3, select_input);
            select_input[2] = "&provinsi=" + prov_select;
            prov_select ? '' : select_input.splice(2, 1);
        }
    }

    // let new_src_iframe_benih = src_iframe_benih + "?bulan=" + date.start_date + "~" + date.end_Date;
    let new_src_iframe_benih = src_iframe_benih + select_input.join("");
    console.log(new_src_iframe_benih);
    $("#iframe-329").attr("src", new_src_iframe_benih);
}

function changeIframeTahun() {
    // console.log(date);
    select_input.map((val, index) => {
        if (val.includes('provinsi')) {
            removeElement(index, select_input);
        } else if (val.includes('bulan')) {
            removeElement(index, select_input);
        }
    })
    getBulan()
    getProvinsi()
    let kab_select = $("#tahun_pil").val();

    if (select_input.length == 0) {
        let tmp = kab_select ? "?tahun=" + kab_select : '';
        select_input.push(tmp);
    } else if (select_input.length == 1) {
        if (select_input[0].includes("?")) {
            if (select_input[0].includes("tahun")) {
                select_input[0] = "?tahun=" + kab_select;
                kab_select ? '' : select_input.splice(0, 1);
            } else if (select_input[0].includes("provinsi") || select_input[0].includes("bulan")) {
                removeElement(1, select_input);
                select_input.push("&tahun=" + kab_select);
                kab_select ? '' : select_input.splice(1, 1);
            } else {
                console.log("Error di changeiframe kabupaten = " + select_input)
            }
        } else {
            console.log("Error di findElement = '?' saat length == 1, select_input = " + select_input)
        }
    } else if (select_input.length == 2) {
        if ((select_input[0].includes("?bulan") && select_input[1].includes("&provinsi")) ||
            (select_input[0].includes("?provinsi") && select_input[1].includes("&bulan"))) {
            removeElement(2, select_input);
            select_input.push("&tahun=" + kab_select);
        } else if (select_input[0].includes("?tahun")) {
            select_input[0] = "?tahun=" + kab_select;
            kab_select ? '' : select_input.splice(0, 1);
            select_input[0] = select_input[0].replace(/[&]/g, "?");
            console.log("masuk ?tahun");
        } else if (select_input[1].includes("&tahun")) {
            select_input[1] = "&tahun=" + kab_select;
            kab_select ? '' : select_input.splice(1, 1);
        }
        // alert("when length == 2, arr select_input = " + select_input);
    } else if (select_input.length == 3) {
        if (select_input[0].includes("?tahun") && !select_input[1].includes("&tahun") &&
            !select_input[2].includes("&tahun")) {

            removeElement(3, select_input);
            select_input[0] = "?tahun=" + kab_select;
            kab_select ? '' : select_input.splice(0, 1);
            select_input[0] = select_input[0].replace(/[&]/g, "?");
        } else if (!select_input[0].includes("?tahun") && select_input[1].includes("&tahun") &&
            !select_input[2].includes("&tahun")) {

            removeElement(3, select_input);
            select_input[1] = "&tahun=" + kab_select;
            kab_select ? '' : select_input.splice(1, 1);
        } else if (!select_input[0].includes("?tahun") && !select_input[1].includes("&tahun") &&
            select_input[2].includes("&tahun")) {

            removeElement(3, select_input);
            select_input[2] = "&tahun=" + kab_select;
            kab_select ? '' : select_input.splice(2, 1);
        }
    }

    // let new_src_iframe_benih = src_iframe_benih + "?bulan=" + date.start_date + "~" + date.end_Date;
    let new_src_iframe_benih = src_iframe_benih + select_input.join("");
    console.log(new_src_iframe_benih);
    $("#iframe-329").attr("src", new_src_iframe_benih);
}

function getBulan() {

    $.ajax({
        url: "/bigenvelope/api/call/id/334",
        method: "post",
        data: {
            tahun_pil: $("#tahun_pil").val(),
            item: 'bulan'
        },
        beforeSend: function () {
            $("#bulan_pil").selectpicker("destroy");
            $("#bulan_pil").html(null);
        },
        success: function (res) {
            console.log(res);
            if (res.transaction) {
                $("#bulan_pil").append('<option value="">All Bulan</option>');
                $.each(res.data.bulan, function (k, v) {
                    $("#bulan_pil").append('<option value="' + v.bulan_text + '">' + v.bulan_text + '</option>');
                });
                $("#bulan_pil").selectpicker();
            }
        }
    });

}

function getProvinsi() {

    $.ajax({
        url: "/bigenvelope/api/call/id/334",
        method: "post",
        data: {
            tahun_pil: $("#tahun_pil").val(),
            bulan_pil: $("#bulan_pil").val(),
            item: 'provinsi'
        },
        beforeSend: function () {
            $("#provinsi_pil").selectpicker("destroy");
            $("#provinsi_pil").html(null);
        },
        success: function (res) {
            if (res.transaction) {
                $("#provinsi_pil").append('<option value="">All Provinsi</option>');
                $.each(res.data.provinsi, function (k, v) {
                    $("#provinsi_pil").append('<option value="' + v.provinsi + '">' + v.provinsi + '</option>');
                });
                $("#provinsi_pil").selectpicker();
            }
        }
    });

}