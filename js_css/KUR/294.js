const url_server_prov = "/assets/kementan/geojson_kementan/DB_PROP.json";
const url_server_kabkot = "/assets/kementan/geojson_kementan/DB_KAB.json";
const src_pie_perSubsektor = "https://10.1.231.160:8443/public/dashboard/d2959bb2-dddb-483a-ab07-4c8385f7b177";
const src_pie_perBank = "https://10.1.231.160:8443/public/dashboard/6f0f2051-c66e-447d-a319-f92d6adc390a";
const src_KUR_utama = "https://10.1.231.160:8443/public/dashboard/be44f548-2145-4a5a-ae23-9cbac9a0659e";
const zoom_view = 4;
var lft_map_kur;
var json_data_kur; // layer highlight untuk leaflet
var json_data;
var select_input = []; // select input utama (kontrol url)
var pie_subsektor_input = []; // select input untuk pie subsektor (diambil dari select input)
var pie_bank_input; // select input untuk pie bank (diambil dari select input)
var polygon_kur = [];
var searcbtahun, searcbbulan, searcprobkab = ""
var pie_pil = "bank"
$(function () {
    $(".m-selectpicker").selectpicker();
    initMap();

    $("#pie_pil").on('change', changePieKur);
})

function removeElement(limit, arr) {
    while (arr.length > limit) {
        arr.pop();
    }
}

function initMap() {
    const default_coord1 = ["Center", -2.600029, 118.015776];
    // leaflet init: id_html -> unntuk selector id tag di html
    // set_view -> untuk assign koordinat default ketika map di-load
    // zoom -> untuk set zoom level default ketika map di-load
    lft_map_opt = L.map('map-kur', {
        minZoom: 4
    }).setView([default_coord1[1], default_coord1[2]], zoom_view);

    // OpenStreetMap init
    const OpenStreetMapUrl = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";

    const OpenStreetMapTileOPT = L.tileLayer(OpenStreetMapUrl, {
        minZoom: 1,
        maxZoom: 20,
        attribution: "<a href='http://www.openstreetmap.org'> &copy;OpenStreetMap </a>, <a href='http://www.pertanian.go.id'> Kementan </a>"
    });

    OpenStreetMapTileOPT.addTo(lft_map_opt);

    // tambah layer untuk highlight
    json_data_kur = L.geoJSON().addTo(lft_map_opt);
}

function changetahun(result) {
    // console.log(result)
    searcbtahun = result ? "tahun=" + result : ""
    changeIframeTemplate()

}

function changebulan(result) {
    // console.log(result)
    searcbbulan = result ? "bulan=" + result : ""
    changeIframeTemplate()
}

function changeprovinsi(result) {
    // console.log(result)
    let tmp = result.split(",")[1].split(" ").join("%20");
    searcprobkab = result.split(",")[1] ? "provinsi___kabupaten=" + tmp : "";
    changeIframeTemplate();
    url = result.split(",")[0].length > 2 ? url_server_kabkot : url_server_prov;
    console.log("url prov => ", url);
    administratif = result.split(",")[0].length > 2 ? "kab/kota" : "provinsi";
    searchGeoJson(url, administratif, result.split(",")[0]);
}

function changerealisasi(result) {
    pie_pil = result
    template = ""
    template += searcbtahun || searcbbulan || searcprobkab ? "?" : "";
    template += searcbtahun ? searcbtahun + "&" : "";
    template += searcbbulan ? searcbbulan + "&" : "";
    template += searcprobkab ? searcprobkab + "&" : "";
    $("pie-penyaluran").text(pie_pil === "bank" ? "Realisasi per Bank Penyalur" : "Realisasi per Subsektor")
    if (pie_pil === "bank") {
        $("#iframe-bottom").attr("src", src_pie_perBank + template);
    } else if (pie_pil === "subsektor") {
        $("#iframe-bottom").attr("src", src_pie_perSubsektor + template);
    }
}

function changePieKur() {
    let pie_val = $("#pie_pil").val();

    if (pie_val === "bank") {
        tmp_title = "Realisasi KUR per Bank Penyalur";
        $("#pie-penyaluran").html(tmp_title);

    } else if (pie_val === "subsektor") {
        tmp_title = "Realisasi KUR per Subsektor";
        $("#pie-penyaluran").html(tmp_title);
    }
}

function changeIframeTemplate() {
    template = ""
    template += searcbtahun || searcbbulan || searcprobkab ? "?" : "";
    template += searcbtahun ? searcbtahun + "&" : "";
    template += searcbbulan ? searcbbulan + "&" : "";
    template += searcprobkab ? searcprobkab + "&" : "";

    // console.log(template);

    if (pie_pil === "bank") {
        $("#iframe-bottom").attr("src", src_pie_perBank + template);
    } else if (pie_pil === "subsektor") {
        $("#iframe-bottom").attr("src", src_pie_perSubsektor + template);
    }
    $("#iframe-left").attr("src", src_KUR_utama + template);
}

function changeIframeRealisasi() {

}

function searchGeoJson(url, administratif, searchDaerah) {
    if (searchDaerah) {
        $.ajax({
            url: url,
            dataType: "json",
            success: function (res) {
                console.log("ajax masuk", res);
                var mark = 0;
                var geo_data = {};
                $.each(res.features, function (key, val) {
                    if (administratif === "provinsi") {
                        if (val.properties['IDADMIN'].toLowerCase() == searchDaerah.toLowerCase()) {
                            console.log("masuk search prov");
                            mark = 1;
                            geo_data = {
                                "type": "Feature",
                                "properties": {
                                    "administrative": administratif
                                },
                                "geometry": {
                                    "type": val.geometry['type'],
                                    "coordinates": val.geometry['coordinates']
                                }
                            };
                        }
                    } else if (administratif === "kab/kota") {
                        if (val.properties['idadmin'].toLowerCase() == searchDaerah.toLowerCase()) {
                            console.log("masuk search kabkot");
                            mark = 1;
                            geo_data = {
                                "type": "Feature",
                                "properties": {
                                    "administrative": administratif
                                },
                                "geometry": {
                                    "type": val.geometry['type'],
                                    "coordinates": val.geometry['coordinates']
                                }
                            };
                        }
                    }
                })
                if (mark === 0) {
                    toastr["error"]("Geometry map data not found", "Alert!!");

                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }

                } else {
                    if (json_data_kur && geo_data) {
                        evaluateMapKonsumen(geo_data, searchDaerah, administratif);
                    }
                }
            },
            error: function (xhr, status, error) {
                alert("search geojson ajax gagal");
                console.log(xhr);
                console.log(status);
                console.log(error);
            }
        });
    } else {
        json_data_kur ? json_data_kur.clearLayers() : '';
        if (administratif == "provinsi") {
            polygon_kur = [];
            lft_map_opt.setView([-2.600029, 118.015776], zoom_view);
        } else if (polygon_kur.length == 2) {
            if (administratif == polygon_kur[0].properties.administrative) {
                polygon_kur.splice(0, 1);
            } else if (administratif == polygon_kur[1].properties.administrative) {
                polygon_kur.splice(1, 1);
            }
            json_data_kur.addData(polygon_kur);
            lft_map_opt.fitBounds(json_data_kur.getBounds());
        } else if (polygon_kur.length == 1) {
            polygon_kur.splice(0, 1);
            lft_map_opt.setView([-2.600029, 118.015776], zoom_view);
        }
    }
}

function evaluateMapKonsumen(geoData, search_val, administratif) {
    if (polygon_kur.length == 0) {
        polygon_kur.push(geoData);
        json_data_kur.clearLayers();
        json_data_kur.addData(geoData);
        lft_map_opt.fitBounds(json_data_kur.getBounds());
    } else if (polygon_kur.length == 1) {
        if (search_val == "") {
            polygon_kur.splice(0, 1);
            json_data_kur.clearLayers();
            lft_map_opt.fitBounds(json_data_kur.getBounds());
        } else {
            if (polygon_kur[0].properties.administrative === administratif) {
                polygon_kur[0] = geoData;
                json_data_kur.clearLayers();
                json_data_kur.addData(polygon_kur[0]);
                lft_map_opt.fitBounds(json_data_kur.getBounds());
            } else {
                polygon_kur.push(geoData);
                if (polygon_kur[0].properties.administrative == "kab/kota" &&
                    polygon_kur[1].properties.administrative == "provinsi") {
                    json_data_kur.clearLayers();
                    json_data_kur.addData(polygon_kur[0]);
                    lft_map_opt.fitBounds(json_data_kur.getBounds());
                } else if (polygon_kur[1].properties.administrative == "kab/kota" &&
                    polygon_kur[0].properties.administrative == "provinsi") {
                    json_data_kur.clearLayers();
                    json_data_kur.addData(polygon_kur[1]);
                    lft_map_opt.fitBounds(json_data_kur.getBounds());
                }
            }
        }
    } else if (polygon_kur.length == 2) {
        if (search_val == "") {
            if (polygon_kur[0].properties.administrative === administratif) {
                polygon_kur.splice(0, 1);
            } else if (polygon_kur[1].properties.administrative === administratif) {
                polygon_kur.splice(1, 1);
            }
            json_data_kur.clearLayers();
            json_data_kur.addData(polygon_kur[0]);
            lft_map_opt.fitBounds(json_data_kur.getBounds());
        } else {
            if (polygon_kur[0].properties.administrative === administratif) {
                polygon_kur[0] = geoData;
            } else if (polygon_kur[1].properties.administrative === administratif) {
                polygon_kur[1] = geoData;
            }

            if (administratif === "provinsi") {
                if (polygon_kur[0].properties.administrative == "kab/kota") {
                    polygon_kur.splice(0, 1);
                } else if (polygon_kur[1].properties.administrative == "kab/kota") {
                    polygon_kur.splice(1, 1);
                }
                json_data_kur.clearLayers();
                json_data_kur.addData(polygon_kur);
                lft_map_opt.fitBounds(json_data_kur.getBounds());
            } else if (administratif === "kab/kota") {
                if (polygon_kur[0].properties.administrative == "kab/kota" &&
                    polygon_kur[1].properties.administrative == "provinsi") {
                    json_data_kur.clearLayers();
                    json_data_kur.addData(polygon_kur[0]);
                    lft_map_opt.fitBounds(json_data_kur.getBounds());
                } else if (polygon_kur[1].properties.administrative == "kab/kota" &&
                    polygon_kur[0].properties.administrative == "provinsi") {
                    json_data_kur.clearLayers();
                    json_data_kur.addData(polygon_kur[1]);
                    lft_map_opt.fitBounds(json_data_kur.getBounds());
                }
            }
        }
    }
}

function changeIframeBulan() {
    // console.log(date); 
    getProvinsi();
    getKabupaten();
    let bulan_pil = $("#bulan_pil").val();
    changeIframeTemplate(bulan_pil, 'bulan', 'bulan');

}

function changeIframeTahun() {
    // console.log(date);
    getBulan();
    getProvinsi();
    getKabupaten();
    let tahun_select = $("#tahun_pil").val();
    changeIframeTemplate(tahun_select, 'tahun', 'tahun');

}

function changeIframeProvinsiKabupaten() {
    // console.log(date);
    getKabupaten();
    let tmp = $("#provinsi_pil").val();
    let prov_select = tmp.split(",")[0]
    changeIframeTemplate(prov_select, 'provinsi___kabupaten', 'provinsi');

    if (tmp.length > 2) {
        url = url_server_kabkot;
        let region_name = tmp
        searchGeoJson(url, 'kab/kota', region_name);
    } else {
        url = url_server_prov;
        let region_name = tmp
        searchGeoJson(url, 'provinsi', region_name);
    }
}

function getBulan() {

    $.ajax({
        url: "/bigenvelope/api/call/id/348", // belum diganti
        method: "post",
        data: {
            item: 'bulan',
            tahun_pil: $("#tahun_pil").val()
        },
        beforeSend: function () {
            $("#bulan_pil").selectpicker("destroy");
            $("#bulan_pil").html(null);
        },
        success: function (res) {
            console.log(res);
            if (res.transaction) {
                $("#bulan_pil").append('<option value="">All Bulan</option>');
                $.each(res.data.bulan, function (k, v) {
                    $("#bulan_pil").append('<option value="' + v.bulan_teks + '">' + v.bulan_teks + '</option>');
                });
                $("#bulan_pil").selectpicker();
            }
        }
    });

}

function getProvinsi() {

    $.ajax({
        url: "/bigenvelope/api/call/id/348", // belum diganti
        method: "post",
        data: {
            item: 'provinsi',
            tahun_pil: $("#tahun_pil").val(),
            bulan_pil: $("#bulan_pil").val(),
        },
        beforeSend: function () {
            $("#provinsi_pil").selectpicker("destroy");
            $("#provinsi_pil").html(null);
        },
        success: function (res) {
            if (res.transaction) {
                $("#provinsi_pil").append('<option value="">All Provinsi</option>');
                $.each(res.data.provinsi, function (k, v) {
                    $("#provinsi_pil").append('<option value="' + v.nama_prov + '">' + v.nama_prov + '</option>');
                });
                $("#provinsi_pil").selectpicker();
            }
        }
    });

}

function getKabupaten() {

    $.ajax({
        url: "/bigenvelope/api/call/id/348", // belum diganti
        method: "post",
        data: {
            item: 'kabupaten',
            tahun_pil: $("#tahun_pil").val(),
            bulan_pil: $("#bulan_pil").val(),
            provinsi_pil: $("#provinsi_pil").val()
        },
        beforeSend: function () {
            $("#kab_pil").selectpicker("destroy");
            $("#kab_pil").html(null);
        },
        success: function (res) {
            if (res.transaction) {
                $("#kab_pil").append('<option value="">All Kabupaten/Kota</option>');
                $.each(res.data.kabupaten, function (k, v) {
                    $("#kab_pil").append('<option value="' + v.nama_kab + '">' + v.nama_kab + '</option>');
                });
                $("#kab_pil").selectpicker();
            }
        }
    });

}