$(document).ready(function () {
    var barURA3 = document.getElementById('barUrea3Canvas').getContext('2d');
    window.barUrea3 = new Chart(barURA3, barUrea3Config);
})

var urea3Des3 = 189 / 100 * 7456533 * 205 / 1000, urea3Jan1 = 53874 * 205 / 1000, urea3Jan2 = 92584 * 205 / 1000, urea3Jan3 = 105010 * 205 / 1000,
    urea3Feb1 = 101636 * 205 / 1000, urea3Feb2 = 646655 * 205 / 1000, urea3Feb3 = 1164525 * 205 / 1000, urea3Mar1 = 867645 * 205 / 1000,
    urea3Mar2 = 701951 * 205 / 1000, urea3Mar3 = 197444 * 205 / 1000, urea3Apr1 = 259 / 100 * 7456533 * 205 / 1000, urea3Apr2 = 307 / 100 * 7456533 * 205 / 1000,
    urea3Apr3 = 120 / 100 * 7456533 * 205 / 1000;
var ZA3Des3 = 189 / 100 * 7456533 * 42 / 1000, ZA3Jan1 = 53874 * 42 / 1000, ZA3Jan2 = 92584 * 42 / 1000, ZA3Jan3 = 105010 * 42 / 1000,
    ZA3Feb1 = 101636 * 42 / 1000, ZA3Feb2 = 646655 * 42 / 1000, ZA3Feb3 = 1164525 * 42 / 1000, ZA3Mar1 = 867645 * 42 / 1000,
    ZA3Mar2 = 701951 * 42 / 1000, ZA3Mar3 = 197444 * 42 / 1000, ZA3Apr1 = 259 / 100 * 7456533 * 42 / 1000, ZA3Apr2 = 307 / 100 * 7456533 * 42 / 1000,
    ZA3Apr3 = 120 / 100 * 7456533 * 42 / 1000;
var SP363Des3 = 189 / 100 * 7456533 * 78 / 1000, SP363Jan1 = 53874 * 78 / 1000, SP363Jan2 = 92584 * 78 / 1000, SP363Jan3 = 105010 * 78 / 1000,
    SP363Feb1 = 101636 * 78 / 1000, SP363Feb2 = 646655 * 78 / 1000, SP363Feb3 = 1164525 * 78 / 1000, SP363Mar1 = 867645 * 78 / 1000,
    SP363Mar2 = 701951 * 78 / 1000, SP363Mar3 = 197444 * 78 / 1000, SP363Apr1 = 259 / 100 * 7456533 * 78 / 1000, SP363Apr2 = 307 / 100 * 7456533 * 78 / 1000,
    SP363Apr3 = 120 / 100 * 7456533 * 78 / 1000;
var KCl3Des3 = 189 / 100 * 7456533 * 67 / 1000, KCl3Jan1 = 53874 * 67 / 1000, KCl3Jan2 = 92584 * 67 / 1000, KCl3Jan3 = 105010 * 67 / 1000,
    KCl3Feb1 = 101636 * 67 / 1000, KCl3Feb2 = 646655 * 67 / 1000, KCl3Feb3 = 1164525 * 67 / 1000, KCl3Mar1 = 867645 * 67 / 1000,
    KCl3Mar2 = 701951 * 67 / 1000, KCl3Mar3 = 197444 * 67 / 1000, KCl3Apr1 = 259 / 100 * 7456533 * 67 / 1000, KCl3Apr2 = 307 / 100 * 7456533 * 67 / 1000,
    KCl3Apr3 = 120 / 100 * 7456533 * 67 / 1000;

var barUrea3Config = {
    type: 'line',
    //plugins: [ChartDataLabels],
    data: {
        labels: ["JUN 2020", "JUL 2020", "AGS 2020"],// "APR 2020"],

        datasets: [{
            label: 'Urea',
            fill: false,
            borderColor: 'rgba(0,0,0,1)',
            backgroundColor: 'rgba(0,0,0,1)',
            data: [urea3Jan1 + urea3Jan2 + urea3Jan3,
            urea3Feb1 + urea3Feb2 + urea3Feb3,
            urea3Mar1 + urea3Mar2 + urea3Mar3],
            //urea3Apr1+urea3Apr2+urea3Apr3],
            datalabels: {
                display: true,
                anchor: 'end',
                align: 'top',
                backgroundColor: 'rgba(255,255,255,0.7)',
                formatter: function (value, context) {
                    return Number(value).toLocaleString("ID", { maximumFractionDigits: 0 });
                },
                display: 'auto'
            }
        }, {
            label: 'ZA',
            fill: false,
            borderColor: 'rgba(0,0,255,1)',
            backgroundColor: 'rgba(0,0,255,1)',
            data: [ZA3Jan1 + ZA3Jan2 + ZA3Jan3,
            ZA3Feb1 + ZA3Feb2 + ZA3Feb3,
            ZA3Mar1 + ZA3Mar2 + ZA3Mar3],
            //ZA3Apr1+ZA3Apr2+ZA3Apr3],
            datalabels: {
                display: true,
                anchor: 'end',
                align: 'top',
                backgroundColor: 'rgba(255,255,255,0.7)',
                formatter: function (value, context) {
                    return Number(value).toLocaleString("ID", { maximumFractionDigits: 0 });
                },
                display: 'auto'
            }
        }, {
            label: 'SP36',
            fill: false,
            borderColor: 'rgba(0,255,255,1)',
            backgroundColor: 'rgba(0,255,255,1)',
            data: [SP363Jan1 + SP363Jan2 + SP363Jan3,
            SP363Feb1 + SP363Feb2 + SP363Feb3,
            SP363Mar1 + SP363Mar2 + SP363Mar3],
            //SP363Apr1+SP363Apr2+SP363Apr3],
            datalabels: {
                display: true,
                anchor: 'end',
                align: 'top',
                backgroundColor: 'rgba(255,255,255,0.7)',
                formatter: function (value, context) {
                    return Number(value).toLocaleString("ID", { maximumFractionDigits: 0 });
                },
                display: 'auto'
            }
        }, {
            label: 'KCl',
            fill: false,
            borderColor: 'rgba(255,255,0,1)',
            backgroundColor: 'rgba(255,255,0,1)',
            data: [KCl3Jan1 + KCl3Jan2 + KCl3Jan3,
            KCl3Feb1 + KCl3Feb2 + KCl3Feb3,
            KCl3Mar1 + KCl3Mar2 + KCl3Mar3],
            //KCl3Apr1+KCl3Apr2+KCl3Apr3],
            datalabels: {
                display: true,
                anchor: 'end',
                align: 'top',
                backgroundColor: 'rgba(255,255,255,0.7)',
                formatter: function (value, context) {
                    return Number(value).toLocaleString("ID", { maximumFractionDigits: 0 });
                },
                display: 'auto'
            }
        }]

    },
    options: {
        responsive: false,
        maintainAspectRatio: false,
        legend: {
            display: false,
            fontSize: 9
        },
        tooltips: {
            enabled: false
        },
        title: {
            display: true,
            text: ['Estimasi Pupuk', ''],
            fontSize: 20
        },
        scales: {
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Ton',
                    fontSize: 20
                },
                ticks: {
                    maxTicksLimit: 5,
                    callback: function (value, index, values) {
                        return value.toLocaleString("ID", { maximumFractionDigits: 0 });
                    }
                }
            }]
        }
    }
};