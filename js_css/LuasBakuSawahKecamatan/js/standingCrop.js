$(document).ready(function () {
    var ctx = document.getElementById('barSCCanvas').getContext('2d');
    window.barSC = new Chart(ctx, barSCConfig);
});

var scbera = 2719871, scair = 207973, scveg = 1542074, scgen = 863248, scripe = 2128337;
var sumsc = scbera + scair + scveg + scgen + scripe;
var barSCConfig = {
    type: 'bar',
    plugins: [ChartDataLabels],
    data: {
        labels: ["Bera", "Pengairan", "Vegetatif", "Reproduktif", "Pemasakan"],
        datasets: [{
            backgroundColor: ["rgb(168, 56, 0)", "rgb(0, 112, 255)", "rgb(163, 255, 115)", "rgb(56, 168, 0)", "rgb(255, 170, 0)"],
            data: [scbera.toFixed(2), scair.toFixed(2), scveg.toFixed(2), scgen.toFixed(2), scripe.toFixed(2)],
            datalabels: {
                display: true,
                anchor: 'end',
                align: 'top',
                formatter: function (value, context) {
                    return Number(value).toLocaleString("ID", { maximumFractionDigits: 0 });
                }
            }
        }]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        tooltips: {
            enabled: false
        },
        title: {
            display: true,
            text: ["Standing Crop SENTINEL-2", "(Citra Per Nov20, non awan)", " "],
            fontSize: 20

        },
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Ha',
                    fontSize: 20
                },
                ticks: {
                    maxTicksLimit: 5,
                    callback: function (value, index, values) {
                        return value.toLocaleString("ID", { maximumFractionDigits: 0 });
                    }
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Total: ' + sumsc.toLocaleString("ID", { maximumFractionDigits: 0 }) + ' Ha',
                    fontSize: 20
                },
                ticks: {
                    fontSize: '11'
                }
            }]
        }

    }
};

// var scbera = 1262003, scair = 576031, scveg = 2431263, scgen = 1421355, scripe = 1771941;
// var sumsc = scbera + scair + scveg + scgen + scripe;
// var barSCConfig = {
//     type: 'bar',
//     plugins: [ChartDataLabels],
//     data: {
//         labels: ["Bera", "Pengairan", "Vegetatif", "Reproduktif", "Pemasakan"],
//         datasets: [{
//             backgroundColor: ["rgb(168, 56, 0)", "rgb(0, 112, 255)", "rgb(163, 255, 115)", "rgb(56, 168, 0)", "rgb(255, 170, 0)"],
//             data: [scbera.toFixed(2), scair.toFixed(2), scveg.toFixed(2), scgen.toFixed(2), scripe.toFixed(2)],
//             datalabels: {
//                 display: true,
//                 anchor: 'end',
//                 align: 'top',
//                 formatter: function (value, context) {
//                     return Number(value).toLocaleString("ID", { maximumFractionDigits: 0 });
//                 }
//             }
//         }]
//     },
//     options: {
//         responsive: true,
//         maintainAspectRatio: false,
//         tooltips: {
//             enabled: false
//         },
//         title: {
//             display: true,
//             text: ["Standing Crop SENTINEL-2", "(Citra Per Jun20, non awan)", " "],
//             fontSize: 20

//         },
//         legend: {
//             display: false
//         },
//         scales: {
//             yAxes: [{
//                 scaleLabel: {
//                     display: true,
//                     labelString: 'Ha',
//                     fontSize: 20
//                 },
//                 ticks: {
//                     maxTicksLimit: 5,
//                     callback: function (value, index, values) {
//                         return value.toLocaleString("ID", { maximumFractionDigits: 0 });
//                     }
//                 }
//             }],
//             xAxes: [{
//                 scaleLabel: {
//                     display: true,
//                     labelString: 'Total: ' + sumsc.toLocaleString("ID", { maximumFractionDigits: 0 }) + ' Ha',
//                     fontSize: 20
//                 },
//                 ticks: {
//                     fontSize: '11'
//                 }
//             }]
//         }

//     }
// };