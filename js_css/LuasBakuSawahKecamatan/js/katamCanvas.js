$(document).ready(function () {
    var barKTM3 = document.getElementById('barKatam3Canvas').getContext('2d');
    window.barKatam3 = new Chart(barKTM3, barKatam3Config);
})

var katam3Des3 = 189 / 100 * 7456533, katam3Jan1 = 53874, katam3Jan2 = 92584, katam3Jan3 = 105010,
    katam3Feb1 = 101636, katam3Feb2 = 646655, katam3Feb3 = 1164525, katam3Mar1 = 867645,
    katam3Mar2 = 701951, katam3Mar3 = 197444, katam3Apr1 = 259 / 100 * 7456533, katam3Apr2 = 307 / 100 * 7456533,
    katam3Apr3 = 120 / 100 * 7456533;

var barKatam3Config = {
    type: 'bar',
    plugins: [ChartDataLabels],
    data: {
        labels: ["JUN.1 20", "JUN.2 20", "JUN.3 20", "JUL.1 20", "JUL.2 20", "JUL.3 20", "AGS.1 20", "AGS.2 20", "AGS.3 20"], //"APR.1 20", "APR.2 20", "APR.3 20"],
        datasets: [{
            backgroundColor: 'rgb(0,88,0,0.5)',
            data: [katam3Jan1, katam3Jan2, katam3Jan3, katam3Feb1, katam3Feb2, katam3Feb3, katam3Mar1, katam3Mar2, katam3Mar3],//katam3Apr1,katam3Apr2,katam3Apr3],
            datalabels: {
                display: true,
                anchor: 'end',
                align: 'top',
                backgroundColor: 'rgba(255,255,255,0.7)',
                formatter: function (value, context) {
                    return Number(value).toLocaleString("ID", { maximumFractionDigits: 0 });
                },
                display: 'auto'
            }

        }]
    },
    options: {
        responsive: true,
        tooltips: {
            enabled: false
        },
        maintainAspectRatio: false,
        legend: {
            display: false
        },
        title: {
            display: true,
            text: ['Kalender Tanam dan Potensi Luas', ''],
            fontSize: 20
        },
        scales: {
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Ha',
                    fontSize: 20
                },
                ticks: {
                    maxTicksLimit: 5,
                    callback: function (value, index, values) {
                        return value.toLocaleString("ID", { maximumFractionDigits: 0 });
                    }
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Dasarian',
                    fontSize: 20
                }
            }]
        }
    }
};