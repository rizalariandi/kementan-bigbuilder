$(document).ready(function () {
    var luasPanenLine = document.getElementById('lineSCCanvas').getContext('2d');
    window.lineSC = new Chart(luasPanenLine, lineSCConfig);
})

var scJan = 2128337,
    scFeb = 863248,
    scMar = 1542074;
//scApr = 292376;
var sumPred = scJan + scFeb + scMar;//+scApr;
var lineSCConfig = {
    type: 'line',
    plugins: [ChartDataLabels],
    data: {
        labels: ["Nov-20", "Des-20", "Jan-20"],//"Apr-20"],
        datasets: [{
            data: [scJan.toFixed(2), scFeb.toFixed(2), scMar.toFixed(2)],//scApr.toFixed(2)],
            label: "",
            borderColor: "#000000",
            fill: true,
            backgroundColor: "rgba(0,255,0,0.3)",
            datalabels: {
                display: true,
                anchor: 'end',
                align: 'top',
                backgroundColor: 'rgba(255,255,255,0.7)',
                formatter: function (value, context) {
                    return Number(value).toLocaleString("ID", { maximumFractionDigits: 0 });
                }
            }
        }
        ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        tooltips: {
            enabled: false
        },
        title: {
            display: true,
            text: ["Estimasi Luas Panen", " "],
            fontSize: 20

        },
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Ha',
                    fontSize: 20
                },
                ticks: {
                    maxTicksLimit: 5,
                    callback: function (value, index, values) {
                        return value.toLocaleString("ID", { maximumFractionDigits: 0 });
                    }
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Total: ' + sumPred.toLocaleString("ID", { maximumFractionDigits: 0 }) + ' Ha',
                    fontSize: 20
                },
                ticks: {
                    //fontSize: '11'
                }
            }]

        }
    }
};

// var scJan = 1771941,
//     scFeb = 1421355,
//     scMar = 3007294;
// //scApr = 292376;
// var sumPred = scJan + scFeb + scMar;//+scApr;
// var lineSCConfig = {
//     type: 'line',
//     plugins: [ChartDataLabels],
//     data: {
//         labels: ["Jun-20", "Jul-20", "Ags-20"],//"Apr-20"],
//         datasets: [{
//             data: [scJan.toFixed(2), scFeb.toFixed(2), scMar.toFixed(2)],//scApr.toFixed(2)],
//             label: "",
//             borderColor: "#000000",
//             fill: true,
//             backgroundColor: "rgba(0,255,0,0.3)",
//             datalabels: {
//                 display: true,
//                 anchor: 'end',
//                 align: 'top',
//                 backgroundColor: 'rgba(255,255,255,0.7)',
//                 formatter: function (value, context) {
//                     return Number(value).toLocaleString("ID", { maximumFractionDigits: 0 });
//                 }
//             }
//         }
//         ]
//     },
//     options: {
//         responsive: true,
//         maintainAspectRatio: false,
//         tooltips: {
//             enabled: false
//         },
//         title: {
//             display: true,
//             text: ["Estimasi Luas Panen", " "],
//             fontSize: 20

//         },
//         legend: {
//             display: false
//         },
//         scales: {
//             yAxes: [{
//                 scaleLabel: {
//                     display: true,
//                     labelString: 'Ha',
//                     fontSize: 20
//                 },
//                 ticks: {
//                     maxTicksLimit: 5,
//                     callback: function (value, index, values) {
//                         return value.toLocaleString("ID", { maximumFractionDigits: 0 });
//                     }
//                 }
//             }],
//             xAxes: [{
//                 scaleLabel: {
//                     display: true,
//                     labelString: 'Total: ' + sumPred.toLocaleString("ID", { maximumFractionDigits: 0 }) + ' Ha',
//                     fontSize: 20
//                 },
//                 ticks: {
//                     //fontSize: '11'
//                 }
//             }]

//         }
//     }
// };