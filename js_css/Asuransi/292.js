const src_luas_lahan = "https://10.1.231.160:8443/public/dashboard/de229096-6a33-4d3f-954a-287f5d3b8cf9";
const src_jumlah_sapi = "https://10.1.231.160:8443/public/dashboard/53cd63d4-ba83-49da-8787-2e044f6c3c28";
const src_nilai_klaim_asuransi_peternak = "https://10.1.231.160:8443/public/dashboard/ba4d3974-faf3-42f6-b2a6-605e1b2c4656";
const src_nilai_klaim_asuransi_pertanian = "https://10.1.231.160:8443/public/dashboard/b82d2a6d-1dc6-4741-88dd-d2a41749d720";
const src_nilai_klaim_dibayar_peternak = "https://10.1.231.160:8443/public/dashboard/be824bec-3822-4dfa-8cc9-dbff795fcfae";
const scr_nilai_klaim_dibayar_pertanian = "https://10.1.231.160:8443/public/dashboard/1331f5e9-1a9c-4c9d-9446-a612d98e2ead";
var autp_auts = "auts"
$(document).ready(function () {
    $(".m-selectpicker").selectpicker();

    $("#NIK-search").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $('#myModal').modal('show');
            getlistasuransi();
        }
    });

    if (autp_auts == "autp") {
        $(".luas_lahan_row").show();
        $(".jumlah_ternak_row").hide();
    } else if (autp_auts == "auts") {
        $(".luas_lahan_row").hide();
        $(".jumlah_ternak_row").show();
    }
})

function changejenis(result) {
    changeiframe(result.value)
    autp_auts = result.value
    $('#tahun').val('').selectpicker('refresh');

    if (autp_auts == "autp") {
        $(".luas_lahan_row").show();
        $(".jumlah_ternak_row").hide();
    } else if (autp_auts == "auts") {
        $(".luas_lahan_row").hide();
        $(".jumlah_ternak_row").show();
    }

    emptyDetail();
}

function changetahun(result) {
    changeiframe(autp_auts, result.value)
}

function changeiframe(result, value_tahun) {
    $("#iframe-line")
    if (result == 'autp') {
        $("#text2").text("Luas Lahan")
        // autp -> pertanian
        // if (!value_tahun) {
        //     $("#iframe-line").attr("src", scr_nilai_klaim_dibayar_pertanian);
        // }
        let addon = value_tahun ? "?tahun=" + value_tahun + "#bordered=false" : "#bordered=false";
        $("#iframe-line").attr("src", scr_nilai_klaim_dibayar_pertanian + addon);
        $("#iframe-asuransi").attr("src", src_nilai_klaim_asuransi_pertanian + addon);
        $("#iframe-bottom").attr("src", src_luas_lahan + addon);

        console.log(src_nilai_klaim_dibayar_peternak + addon);
    } else if (result == 'auts') {
        $("#text2").text("Jumlah Sapi")
        // auts -> peternakan
        // if (!value_tahun) {
        //     $("#iframe-line").attr("src", src_nilai_klaim_dibayar_peternak);
        // }
        let addon = value_tahun ? "?tahun=" + value_tahun + "#bordered=false" : "#bordered=false";
        $("#iframe-line").attr("src", src_nilai_klaim_dibayar_peternak + addon);
        $("#iframe-asuransi").attr("src", src_nilai_klaim_asuransi_peternak + addon);
        $("#iframe-bottom").attr("src", src_jumlah_sapi + addon);

        console.log(src_nilai_klaim_dibayar_peternak + addon);
    }

}

function getlistasuransi() {
    $.ajax({
        url: "/bigenvelope/api/call/id/2597",
        method: "post",
        data: {
            item: 'list',
            search: $('#NIK-search').val(),
            autp_auts: autp_auts
        },
        beforeSend: function () {
            $(".modal-body").empty();
            Swal.fire({
                icon: 'info',          
                text: 'Sedang proses',
                allowOutsideClick: false,
                showConfirmButton: false,
            });
            // 
        },
        success: function (res) {
            Swal.close();
            if (res.transaction){
                if (res.data.length > 0) {
                    $.each(res.data, function (k, v) {
                        $(".modal-body").append("<button style='margin:5px' type='button' onclick='getdetailasuransi(this)' class='btn btn-primary'>" + v.nik + "</button>")
                    });
                }
                else {
                    $(".modal-body").append(" <table id='modal-table'><tr><td colspan='0>No results</td></tr></table>")
                }
            }
            
        }
    });

}

function getdetailasuransi(search) {
    search = search.textContent;
    $('#myModal').modal('hide');
    $('#NIK-search').val(search);
    $.ajax({
        url: "/bigenvelope/api/call/id/2597",
        method: "post",
        data: {
            item: 'detail',
            search: search,
            autp_auts: autp_auts
        },
        beforeSend: function () {
            // 
        },
        success: function (res) {
            if (res.transaction) {
                $(".nik").text(res.data.nik);
                if (autp_auts == "autp") {
                    $(".nama").text(res.data.nama_petani);
                } else if (autp_auts == "auts") {
                    $(".nama").text(res.data.nama_peternak);
                }                                                      // waktu autp nama petani, waktu auts nama peternak
                $(".nama_kel").text(res.data.nama_poktan);
                $(".prov").text(res.data.nama_provinsi);
                $(".kabkat").text(res.data.nama_kota);
                $(".kec").text(res.data.nama_kecamatan);
                if (autp_auts == "autp") {
                    $(".jumlah_ternak_row").hide();
                    $(".luas_lahan_row").show();
                    $(".luas_lahan").text(Number(res.data.luas_lahan_polis).toFixed(0) + " hektar");
                } else if (autp_auts == "auts") {
                    $(".luas_lahan_row").hide();
                    $(".jumlah_ternak_row").show();
                    $(".jumlah_ternak").text(res.data.jumlah_sapi_polis ? Number(res.data.jumlah_sapi_polis).toFixed(0) + " ekor sapi" : "-");
                }
                $(".tanggal_klaim").text(res.data.tgl_klaim_terakhir ? res.data.tgl_klaim_terakhir : "-");
                $(".nilai_klaim").text(res.data.nilai_klaim_terakhir ? "Rp." + res.data.nilai_klaim_terakhir.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") : '-');
                $(".no_polis").text(res.data.nomor_polis_terakhir ? res.data.nomor_polis_terakhir : '-');
                $(".total_klaim").text(res.data.total_klaim ? "Rp." + res.data.total_klaim.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") : '-');                      // diberi satuan rupiah dan separator
            }
        }
    });

}

function emptyDetail() {
    $(".nik").text("-");
    $(".nama").text("-");                                                    // waktu autp nama petani, waktu auts nama peternak
    $(".nama_kel").text("-");
    $(".prov").text("-");
    $(".kabkat").text("-");
    $(".kec").text("-");
    if (autp_auts == "autp") {
        $(".jumlah_ternak_row").hide();
        $(".luas_lahan_row").show();
        $(".luas_lahan").text("-");
    } else if (autp_auts == "auts") {
        $(".luas_lahan_row").hide();
        $(".jumlah_ternak_row").show();
        $(".jumlah_ternak").text("-");
    }
    $(".tanggal_klaim").text("-");
    $(".nilai_klaim").text('-');
    $(".no_polis").text('-');
    $(".total_klaim").text('-');
}