let src_iframe_pemotongan = "https://10.1.231.160:8443/public/dashboard/4548331b-4162-417b-90d7-e3f5712e24ea";
var select_input = []
var dateStorage = {};
$(function () {
    $(".m-selectpicker").selectpicker();
    datePicker();

    $("#provinsi_1").on("change", changeIframeProvinsi);
    $("#kab_1").on("change", changeIframeKabupaten);
})

function removeElement(limit, arr) {
    while (arr.length > limit) {
        arr.pop();
    }
}

function datePicker() {
    var date = {};

    $("#kt_daterangepicker_2").daterangepicker({
            autoApply: true,
            startDate: moment().subtract(30, 'days'),
            endDate: moment(),
            maxDate: moment()
        },
        function (start, end) {
            date = {
                'start_date': start.format('YYYY-MM-DD'),
                'end_Date': end.format('YYYY-MM-DD')
            }
            $("#txt-date").val(date.start_date + " hingga " + date.end_Date);
            changeIframeDate(date);
            // console.log(date);
        }
    );
    date = {
        'start_date': moment().subtract(30, 'days').format('YYYY-MM-DD'),
        'end_Date': moment().format('YYYY-MM-DD')
    }
    $("#txt-date").val(date.start_date + " hingga " + date.end_Date);
    changeIframeDate(date);
}

function changeIframeDate(date) {
    // console.log(date);
    getProvinsi(date);
    getKabupaten();
    select_input.map((val, index) => {
        if (val.includes('provinsi')) {
            removeElement(index, select_input);
        } else if (val.includes('kabupaten_kota')) {
            removeElement(index, select_input);
        }
    })
    if (select_input.length == 0) {
        // let tmp = "?tanggal=" + date.start_date + "~" + date.end_Date;
        let tmp = date ? "?tanggal=" + date.start_date + "~" + date.end_Date : '';
        select_input.push(tmp);
    } else if (select_input.length == 1) {
        if (select_input[0].includes("?tanggal")) {
            select_input[0] = "?tanggal=" + date.start_date + "~" + date.end_Date;
            date ? {} : select_input.splice(0, 1); // cek lagi
        } else if (select_input[0].includes("?kabupaten_kota") || select_input[0].includes("?provinsi")) {
            removeElement(1, select_input);
            select_input.push("&tanggal=" + date.start_date + "~" + date.end_Date);
            date ? {} : select_input.splice(1, 1);
        } else {
            console.log("Error di changeiframedate= " + select_input)
        }
    } else if (select_input.length == 2) {
        if ((select_input[0].includes("?provinsi") && select_input[1].includes("&kabupaten_kota")) ||
            (select_input[0].includes("?kabupaten_kota") && select_input[1].includes("&provinsi"))) {
            removeElement(2, select_input);
            select_input.push("&tanggal=" + date.start_date + "~" + date.end_Date);
        } else if (select_input[0].includes("?tanggal")) {
            select_input[0] = "?tanggal=" + date.start_date + "~" + date.end_Date;
            date ? {} : select_input.splice(0, 1);
            select_input[0] = select_input[0].replace(/[&]/g, "?");
        } else if (select_input[1].includes("&tanggal")) {
            select_input[1] = "&tanggal=" + date.start_date + "~" + date.end_Date;
            date ? {} : select_input.splice(1, 1);
        }
    } else if (select_input.length == 3) {
        if (select_input[0].includes("?tanggal") && !select_input[1].includes("&tanggal") && !select_input[2].includes("&tanggal")) {
            removeElement(3, select_input);
            select_input[0] = "?tanggal=" + date.start_date + "~" + date.end_Date;
            date ? {} : select_input.splice(0, 1);
            select_input[0] = select_input[0].replace(/[&]/g, "?");
        } else if (!select_input[0].includes("?tanggal") && select_input[1].includes("&tanggal") && !select_input[2].includes("&tanggal")) {
            removeElement(3, select_input);
            select_input[1] = "&tanggal=" + date.start_date + "~" + date.end_Date;
            date ? {} : select_input.splice(1, 1);
        } else if (!select_input[0].includes("?tanggal") && !select_input[1].includes("&tanggal") && select_input[2].includes("&tanggal")) {
            removeElement(3, select_input);
            select_input[2] = "&tanggal=" + date.start_date + "~" + date.end_Date;
            date ? {} : select_input.splice(2, 1);
        }
    }

    // let new_src_iframe_pemotongan = src_iframe_pemotongan + "?tanggal=" + date.start_date + "~" + date.end_Date;
    let new_src_iframe_pemotongan = src_iframe_pemotongan + select_input.join("");
    console.log(new_src_iframe_pemotongan);
    $("#portlet-571").attr("src", new_src_iframe_pemotongan);
}

function changeIframeProvinsi() {
    // console.log(date);
    select_input.map((val, index) => {
        if (val.includes('kabupaten_kota')) {
            removeElement(index, select_input);
        }
    })
    getKabupaten();
    let tmp = $("#provinsi_1").val().split(" ");
    let prov_select = tmp.join("%20");

    if (select_input.length == 0) {
        let tmp = prov_select ? "?provinsi=" + prov_select : '';
        select_input.push(tmp);
    } else if (select_input.length == 1) {
        if (select_input[0].includes("?")) {
            if (select_input[0].includes("provinsi")) {
                select_input[0] = "?provinsi=" + prov_select;
                prov_select ? '' : select_input.splice(0, 1);
            } else if (select_input[0].includes("kabupaten_kota") || select_input[0].includes("tanggal")) {
                removeElement(1, select_input);
                select_input.push("&provinsi=" + prov_select);
            } else {
                console.log("Error di changeiframedate= " + select_input)
            }
        } else {
            console.log("Error di findelement = '?' saat length == 1, select_input = " + select_input)
        }
    } else if (select_input.length == 2) {
        if ((select_input[0].includes("?tanggal") && select_input[1].includes("&kabupaten_kota")) ||
            (select_input[0].includes("?kabupaten_kota") && select_input[1].includes("&tanggal"))) {
            removeElement(2, select_input);
            select_input.push("&provinsi=" + prov_select);
        } else if (select_input[0].includes("?provinsi")) {
            select_input[0] = "?provinsi=" + prov_select;
            prov_select ? '' : select_input.splice(0, 1);
            select_input[0] = select_input[0].replace(/[&]/g, "?");
        } else if (select_input[1].includes("&provinsi")) {
            select_input[1] = "&provinsi=" + prov_select;
            prov_select ? '' : select_input.splice(1, 1);
        }
    } else if (select_input.length == 3) {
        if (select_input[0].includes("?provinsi") && !select_input[1].includes("&provinsi") && !select_input[2].includes("&provinsi")) {
            removeElement(3, select_input);
            select_input[0] = "?provinsi=" + prov_select;
            prov_select ? '' : select_input.splice(0, 1);
            select_input[0] = select_input[0].replace(/[&]/g, "?");
        } else if (!select_input[0].includes("?provinsi") && select_input[1].includes("&provinsi") && !select_input[2].includes("&provinsi")) {
            removeElement(3, select_input);
            select_input[1] = "&provinsi=" + prov_select;
            prov_select ? '' : select_input.splice(1, 1);
        } else if (!select_input[0].includes("?provinsi") && !select_input[1].includes("&provinsi") && select_input[2].includes("&provinsi")) {
            removeElement(3, select_input);
            select_input[2] = "&provinsi=" + prov_select;
            prov_select ? '' : select_input.splice(2, 1);
        }
    }

    // let new_src_iframe_pemotongan = src_iframe_pemotongan + "?tanggal=" + date.start_date + "~" + date.end_Date;
    let new_src_iframe_pemotongan = src_iframe_pemotongan + select_input.join("");
    console.log(new_src_iframe_pemotongan);
    $("#portlet-571").attr("src", new_src_iframe_pemotongan);
}

function changeIframeKabupaten() {
    // console.log(date);
    let tmp = $("#kab_1").val().split();
    let kab_select = tmp.join("%20");

    if (select_input.length == 0) {
        let tmp = kab_select ? "?kabupaten_kota=" + kab_select : '';
        select_input.push(tmp);
    } else if (select_input.length == 1) {
        if (select_input[0].includes("?")) {
            if (select_input[0].includes("kabupaten_kota")) {
                select_input[0] = "?kabupaten_kota=" + kab_select;
                kab_select ? '' : select_input.splice(0, 1);
            } else if (select_input[0].includes("provinsi") || select_input[0].includes("tanggal")) {
                removeElement(1, select_input);
                select_input.push("&kabupaten_kota=" + kab_select);
                kab_select ? '' : select_input.splice(1, 1);
            } else {
                console.log("Error di changeiframe kabupaten = " + select_input)
            }
        } else {
            console.log("Error di findElement = '?' saat length == 1, select_input = " + select_input)
        }
    } else if (select_input.length == 2) {
        if ((select_input[0].includes("?tanggal") && select_input[1].includes("&provinsi")) ||
            (select_input[0].includes("?provinsi") && select_input[1].includes("&tanggal"))) {
            removeElement(2, select_input);
            select_input.push("&kabupaten_kota=" + kab_select);
        } else if (select_input[0].includes("?kabupaten_kota")) {
            select_input[0] = "?kabupaten_kota=" + kab_select;
            kab_select ? '' : select_input.splice(0, 1);
            select_input[0] = select_input[0].replace(/[&]/g, "?");
        } else if (select_input[1].includes("&kabupaten_kota")) {
            select_input[1] = "&kabupaten_kota=" + kab_select;
            kab_select ? '' : select_input.splice(1, 1);
        }
    } else if (select_input.length == 3) {
        if (select_input[0].includes("?kabupaten_kota") && !select_input[1].includes("&kabupaten_kota") &&
            !select_input[2].includes("&kabupaten_kota")) {

            removeElement(3, select_input);
            select_input[0] = "?kabupaten_kota=" + kab_select;
            kab_select ? '' : select_input.splice(0, 1);
            select_input[0] = select_input[0].replace(/[&]/g, "?");
        } else if (!select_input[0].includes("?kabupaten_kota") && select_input[1].includes("&kabupaten_kota") &&
            !select_input[2].includes("&kabupaten_kota")) {

            removeElement(3, select_input);
            select_input[1] = "&kabupaten_kota=" + kab_select;
            kab_select ? '' : select_input.splice(1, 1);
        } else if (!select_input[0].includes("?kabupaten_kota") && !select_input[1].includes("&kabupaten_kota") &&
            select_input[2].includes("&kabupaten_kota")) {

            removeElement(3, select_input);
            select_input[2] = "&kabupaten_kota=" + kab_select;
            kab_select ? '' : select_input.splice(2, 1);
        }
    }

    // let new_src_iframe_pemotongan = src_iframe_pemotongan + "?tanggal=" + date.start_date + "~" + date.end_Date;
    let new_src_iframe_pemotongan = src_iframe_pemotongan + select_input.join("");
    $("#portlet-571").attr("src", new_src_iframe_pemotongan);
}

function getProvinsi(date) {
    dateStorage = date;
    $.ajax({
        url: "/bigenvelope/api/call/id/333",
        method: "post",
        data: {
            item: 'provinsi',
            startDate: dateStorage.start_date,
            endDate: dateStorage.end_Date
        },
        beforeSend: function () {
            $("#provinsi_1").selectpicker("destroy");
            $("#provinsi_1").html(null);
        },
        success: function (res) {
            if (res.transaction) {
                $("#provinsi_1").append('<option value="">All Provinsi</option>');
                $.each(res.data.provinsi, function (k, v) {
                    $("#provinsi_1").append('<option value="' + v.provinsi + '">' + v.provinsi + '</option>');
                });
                $("#provinsi_1").selectpicker();
            }
        }
    });

}

function getKabupaten() {

    $.ajax({
        url: "/bigenvelope/api/call/id/333",
        method: "post",
        data: {
            item: 'kabupaten',
            startDate: dateStorage.start_date,
            endDate: dateStorage.end_Date,
            provinsi: $("#provinsi_1").val()
        },
        beforeSend: function () {
            $("#kab_1").selectpicker("destroy");
            $("#kab_1").html(null);
        },
        success: function (res) {
            if (res.transaction) {
                $("#kab_1").append('<option value="">All Kabupaten/Kota</option>');
                $.each(res.data.kabupaten, function (k, v) {
                    $("#kab_1").append('<option value="' + v.kabupaten + '">' + v.kabupaten + '</option>');
                });
                $("#kab_1").selectpicker();
            }
        }
    });

}