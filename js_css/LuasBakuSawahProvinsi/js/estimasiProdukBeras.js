$(document).ready(function () {
    var luasBerasLine = document.getElementById('lineBerasCanvas').getContext('2d');
    window.lineBrs = new Chart(luasBerasLine, lineBerasConfig);
})

var provitas = 5.56;
var predBerasJan = scJan * provitas * 0.9 * 0.6,
    predBerasFeb = scFeb * provitas * 0.9 * 0.6,
    predBerasMar = scMar * provitas * 0.9 * 0.6;
//predBerasApr = scApr*provitas*0.9*0.6;
var sumBers = Math.round(predBerasJan) + Math.round(predBerasFeb) + Math.round(predBerasMar);// + Math.round(predBerasApr);
//document.getElementById("predBeras").innerHTML = "Total <strong>" + sumBers.toLocaleString("ID",{maximumFractionDigits:0}) +"</strong> Ton";
//new Chart(document.getElementById("line-beras"), {
var lineBerasConfig = {
    type: 'line',
    plugins: [ChartDataLabels],
    data: {
        labels: ["Nov-20", "Des-20", "Jan-20"],//"Apr-20"],
        datasets: [{
            data: [predBerasJan.toFixed(2), predBerasFeb.toFixed(2), predBerasMar.toFixed(2)],// predBerasApr.toFixed(2)],
            label: "",
            steppedLine: 'middle',
            borderColor: "#000000",
            fill: true,
            backgroundColor: "rgba(255,255,255,0.75)",
            datalabels: {
                display: true,
                anchor: 'end',
                align: 'top',
                backgroundColor: 'rgba(255,255,255,0.7)',
                formatter: function (value, context) {
                    return Number(value).toLocaleString("ID", { maximumFractionDigits: 0 });
                }
            }

        }
        ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        tooltips: {
            enabled: false
        },
        title: {
            display: true,
            text: ["Estimasi Produksi Beras", " "],
            fontSize: 20

        },

        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Ton',
                    fontSize: 20
                },
                ticks: {
                    maxTicksLimit: 5,
                    callback: function (value, index, values) {
                        return value.toLocaleString("ID", { maximumFractionDigits: 0 });
                    }
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Total: ' + sumBers.toLocaleString("ID", { maximumFractionDigits: 0 }) + ' Ton',
                    fontSize: 20
                },
                ticks: {
                    //fontSize: '11'
                }
            }]

        }

    }
};

// var provitas = 5.56;
// var predBerasJan = scJan * provitas * 0.9 * 0.6,
//     predBerasFeb = scFeb * provitas * 0.9 * 0.6,
//     predBerasMar = scMar * provitas * 0.9 * 0.6;
// //predBerasApr = scApr*provitas*0.9*0.6;
// var sumBers = Math.round(predBerasJan) + Math.round(predBerasFeb) + Math.round(predBerasMar);// + Math.round(predBerasApr);
// //document.getElementById("predBeras").innerHTML = "Total <strong>" + sumBers.toLocaleString("ID",{maximumFractionDigits:0}) +"</strong> Ton";
// //new Chart(document.getElementById("line-beras"), {
// var lineBerasConfig = {
//     type: 'line',
//     plugins: [ChartDataLabels],
//     data: {
//         labels: ["Mei-20", "Jun-20", "Jul-20"],//"Apr-20"],
//         datasets: [{
//             data: [predBerasJan.toFixed(2), predBerasFeb.toFixed(2), predBerasMar.toFixed(2)],// predBerasApr.toFixed(2)],
//             label: "",
//             steppedLine: 'middle',
//             borderColor: "#000000",
//             fill: true,
//             backgroundColor: "rgba(255,255,255,0.75)",
//             datalabels: {
//                 display: true,
//                 anchor: 'end',
//                 align: 'top',
//                 backgroundColor: 'rgba(255,255,255,0.7)',
//                 formatter: function (value, context) {
//                     return Number(value).toLocaleString("ID", { maximumFractionDigits: 0 });
//                 }
//             }

//         }
//         ]
//     },
//     options: {
//         responsive: true,
//         maintainAspectRatio: false,
//         tooltips: {
//             enabled: false
//         },
//         title: {
//             display: true,
//             text: ["Estimasi Produksi Beras", " "],
//             fontSize: 20

//         },

//         legend: {
//             display: false
//         },
//         scales: {
//             yAxes: [{
//                 scaleLabel: {
//                     display: true,
//                     labelString: 'Ton',
//                     fontSize: 20
//                 },
//                 ticks: {
//                     maxTicksLimit: 5,
//                     callback: function (value, index, values) {
//                         return value.toLocaleString("ID", { maximumFractionDigits: 0 });
//                     }
//                 }
//             }],
//             xAxes: [{
//                 scaleLabel: {
//                     display: true,
//                     labelString: 'Total: ' + sumBers.toLocaleString("ID", { maximumFractionDigits: 0 }) + ' Ton',
//                     fontSize: 20
//                 },
//                 ticks: {
//                     //fontSize: '11'
//                 }
//             }]

//         }

//     }
// };