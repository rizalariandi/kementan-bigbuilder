function updateMap() {
    mymap.removeLayer(lbsMap);
    mymap.removeLayer(komoditasMap);
    mymap.removeLayer(komoinvMap);
    mymap.removeLayer(longsorMap);
    mymap.removeLayer(agkMap);
    mymap.removeLayer(adminMap);
    mymap.removeLayer(gambutMap);
    mymap.removeLayer(sisdaMap);
    mymap.removeLayer(sawitMap);
    mymap.removeLayer(tebuMap);
    mymap.removeLayer(psrMap);
    mymap.removeLayer(keringMap);
    mymap.removeLayer(sentinelMap);
    mymap.removeLayer(Esri_WorldImagery);
    mymap.removeLayer(desalayer);
    var peta = document.getElementById("peta");
    switch (peta.options[peta.selectedIndex].value) {
        case "SAWAH":
            mymap.addLayer(lbsMap);
            document.getElementById("legenda").src = "/assets/kementan/map-tiles/lbs/legenda.png";
            break;
        case "SAWIT":
            mymap.addLayer(sawitMap);
            document.getElementById("legenda").src = "/assets/kementan/map-tiles/sawit/legenda.png";
            break;
        case "TEBU":
            mymap.addLayer(tebuMap);
            document.getElementById("legenda").src = "/assets/kementan/map-tiles/tebu/legenda.png";
            break;
        case "PSR":
            mymap.addLayer(psrMap);
            document.getElementById("legenda").src = "/assets/kementan/map-tiles/psr/legenda.png";
            break;
        case "SENTINEL":
            mymap.addLayer(sentinelMap);
            document.getElementById("legenda").src = "/assets/kementan/map-tiles/sentinel/legenda.png";
            if (mymap.getZoom() > 12) {
                if (!mymap.hasLayer(desalayer)) {
                    mymap.addLayer(desalayer);
                }
            };
            break;
        case "KOMO":
            mymap.addLayer(komoditasMap);
            document.getElementById("legenda").src = "/assets/kementan/map-tiles/komoditas/legenda.png";
            break;
        case "KOMOINV":
            mymap.addLayer(komoinvMap);
            document.getElementById("legenda").src = "/assets/kementan/map-tiles/komoinv/legenda.png";
            break;
        case "LONGSOR":
            mymap.addLayer(longsorMap);
            document.getElementById("legenda").src = "/assets/kementan/map-tiles/longsor/legenda.png";
            break;
        case "AGK":
            mymap.addLayer(agkMap);
            document.getElementById("legenda").src = "/assets/kementan/map-tiles/agk/legenda.png";
            break;
        case "ADMIN":
            mymap.addLayer(adminMap);
            document.getElementById("legenda").src = "";
            break;
        case "CITRA":
            mymap.addLayer(Esri_WorldImagery);
            document.getElementById("legenda").src = "";
            break;
        case "GAMBUT":
            mymap.addLayer(gambutMap);
            document.getElementById("legenda").src = "/assets/kementan/map-tiles/gambut2/legenda.png";
            break;
        case "KERING":
            mymap.addLayer(keringMap);
            document.getElementById("legenda").src = "/assets/kementan/map-tiles/kering/legenda.png";
            break;
        case "SISDA":
            mymap.addLayer(sisdaMap);
            document.getElementById("legenda").src = "/assets/kementan/map-tiles/sisda/legenda.png";
            break;
    };
    var zoomlevel = mymap.getZoom();
    if (zoomlevel >= 11) {
        mymap.removeLayer(desaMap);
        mymap.addLayer(desaMap);
    }
};