let src_iframe_pihc = "https://10.1.231.160:8443/public/dashboard/2a265f46-26d0-496f-8e16-52eca1dbf603";
var select_input = []

$(function () {
    $(".m-selectpicker").selectpicker();

    $("#provinsi_pil").on("change", changeIframeProvinsi);
    $("#tahun_pil").on("change", changeIframeTahun);
    $("#bulan_pil").on("change", changeIframeBulan);
    $('#kab_pil').on("change", changeIframeKab)
})

function removeElement(limit, arr) {
    while (arr.length > limit) {
        arr.pop();
    }
}

function changeIframeTemplate(select_id_val, iframe_url_selector, filter_type) {
    let url_selector_idx0 = "?" + iframe_url_selector + "=";
    let url_selector_idxN = "&" + iframe_url_selector + "=";
    if (filter_type == 'tahun') {
        select_input.map((val, index) => {
            if (val.includes('provinsi')) {
                removeElement(index, select_input);
            } else if (val.includes('bulan')) {
                removeElement(index, select_input);
            } else if (val.includes('kabupaten_kota')) {
                removeElement(index, select_input);
            }
        })
    } else if (filter_type == 'bulan') {
        select_input.map((val, index) => {
            if (val.includes('provinsi')) {
                removeElement(index, select_input);
            } else if (val.includes('kabupaten_kota')) {
                removeElement(index, select_input);
            }
        })
    } else if (filter_type == 'provinsi') {
        select_input.map((val, index) => {
            if (val.includes('kabupaten_kota')) {
                removeElement(index, select_input);
            }
        })
    }

    if (select_input.length == 0) {
        let tmp = select_id_val ? url_selector_idx0 + select_id_val : '';
        select_input.push(tmp);
    } else if (select_input.length == 1) {
        if (select_input[0].includes(url_selector_idx0)) {
            select_input[0] = url_selector_idx0 + select_id_val;
            select_id_val ? '' : select_input.splice(0, 1);
        } else if (filter_type == "provinsi") {
            if (select_input[0].includes("?tahun") || select_input[0].includes("?kabupaten_kota") ||
                select_input[0].includes("?bulan")) {
                removeElement(1, select_input);
                select_input.push(url_selector_idxN + select_id_val);
            }
        } else if (filter_type == "kab/kota") {
            if (select_input[0].includes("?tahun") || select_input[0].includes("?provinsi") ||
                select_input[0].includes("?bulan")) {
                removeElement(1, select_input);
                select_input.push(url_selector_idxN + select_id_val);
            }
        } else if (filter_type == "tahun") {
            if (select_input[0].includes("?bulan") || select_input[0].includes("?provinsi") ||
                select_input[0].includes("?kabupaten_kota")) {
                removeElement(1, select_input);
                select_input.push(url_selector_idxN + select_id_val);
            }
        } else if (filter_type == "bulan") {
            if (select_input[0].includes("?tahun") || select_input[0].includes("?provinsi") ||
                select_input[0].includes("?kabupaten_kota")) {
                removeElement(1, select_input);
                select_input.push(url_selector_idxN + select_id_val);
            }
        } else {
            console.log("Error di select input length == 1 => " + select_input)
        }
    } else if (select_input.length == 2) {
        if (select_input[0].includes("?") && select_input[1].includes("&")) {
            if ((!select_input[0].includes(iframe_url_selector) && !select_input[1].includes(iframe_url_selector))) {
                removeElement(2, select_input);
                select_input.push(url_selector_idxN + select_id_val);
            } else if (select_input[0].includes(url_selector_idx0)) {
                select_input[0] = url_selector_idx0 + select_id_val;
                select_id_val ? '' : select_input.splice(0, 1);
                select_input[0] = select_input[0].replace(/[&]/g, "?");
            } else if (select_input[1].includes(url_selector_idxN)) {
                select_input[1] = url_selector_idxN + select_id_val;
                select_id_val ? '' : select_input.splice(1, 1);
            }
        }
        // alert("arr select_input = " + select_input);
    } else if (select_input.length == 3) {
        if (!select_input[0].includes(url_selector_idx0) && !select_input[1].includes(url_selector_idxN) &&
            !select_input[2].includes(url_selector_idxN)) {

            removeElement(3, select_input);
            select_input.push(url_selector_idxN + select_id_val);

        } else if (select_input[0].includes(url_selector_idx0) && !select_input[1].includes(url_selector_idxN) &&
            !select_input[2].includes(url_selector_idxN)) {

            removeElement(3, select_input);
            select_input[0] = url_selector_idx0 + select_id_val;
            select_id_val ? '' : select_input.splice(0, 1);
            select_input[0] = select_input[0].replace(/[&]/g, "?");

        } else if (!select_input[0].includes(url_selector_idx0) && select_input[1].includes(url_selector_idxN) &&
            !select_input[2].includes(url_selector_idxN)) {

            removeElement(3, select_input);
            select_input[1] = url_selector_idxN + select_id_val;
            select_id_val ? '' : select_input.splice(1, 1);

        } else if (!select_input[0].includes(url_selector_idx0) && !select_input[1].includes(url_selector_idxN) &&
            select_input[2].includes(url_selector_idxN)) {

            removeElement(3, select_input);
            select_input[2] = url_selector_idxN + select_id_val;
            select_id_val ? '' : select_input.splice(2, 1);
        }
    } else if (select_input.length == 4) {
        if (select_input[0].includes(url_selector_idx0) && !select_input[1].includes(url_selector_idxN) &&
            !select_input[2].includes(url_selector_idxN) && !select_input[3].includes(url_selector_idxN)) {

            removeElement(4, select_input);
            select_input[0] = url_selector_idx0 + select_id_val;
            select_id_val ? '' : select_input.splice(0, 1);
            select_input[0] = select_input[0].replace(/[&]/g, "?");

        } else if (!select_input[0].includes(url_selector_idx0) && select_input[1].includes(url_selector_idxN) &&
            !select_input[2].includes(url_selector_idxN) && !select_input[3].includes(url_selector_idxN)) {

            removeElement(4, select_input);
            select_input[1] = url_selector_idxN + select_id_val;
            select_id_val ? '' : select_input.splice(1, 1);

        } else if (!select_input[0].includes(url_selector_idx0) && !select_input[1].includes(url_selector_idxN) &&
            select_input[2].includes(url_selector_idxN) && !select_input[3].includes(url_selector_idxN)) {

            removeElement(4, select_input);
            select_input[2] = url_selector_idxN + select_id_val;
            select_id_val ? '' : select_input.splice(2, 1);

        } else if (!select_input[0].includes(url_selector_idx0) && !select_input[1].includes(url_selector_idxN) &&
            !select_input[2].includes(url_selector_idxN) && select_input[3].includes(url_selector_idxN)) {

            removeElement(4, select_input);
            select_input[3] = url_selector_idxN + select_id_val;
            select_id_val ? '' : select_input.splice(3, 1);

        }
    }

    let new_src_iframe_pihc = src_iframe_pihc + select_input.join("");
    console.log(new_src_iframe_pihc);
    $("#iframe-326").attr("src", new_src_iframe_pihc);
    // return select_input.join("");
}

function changeIframeBulan() {
    // console.log(date); 
    getProvinsi();
    getKabupaten();
    let bulan_pil = $("#bulan_pil").val();
    changeIframeTemplate(bulan_pil, 'bulan', 'bulan');

}

function changeIframeTahun() {
    // console.log(date);
    getBulan();
    getProvinsi();
    getKabupaten();
    let tahun_select = $("#tahun_pil").val();
    changeIframeTemplate(tahun_select, 'tahun', 'tahun');

}

function changeIframeProvinsi() {
    // console.log(date);
    getKabupaten();
    let tmp = $("#provinsi_pil").val().split(" ");
    let prov_select = tmp.join("%20");
    changeIframeTemplate(prov_select, 'provinsi', 'provinsi');

}

function changeIframeKab() {
    // console.log(date);
    let tmp = $("#kab_pil").val().split(" ");
    let kab_select = tmp.join("%20");
    changeIframeTemplate(kab_select, 'kabupaten_kota', 'kab/kota');

}

function getBulan() {

    $.ajax({
        url: "/bigenvelope/api/call/id/331",
        method: "post",
        data: {
            item: 'bulan',
            tahun_pil: $("#tahun_pil").val()
        },
        beforeSend: function () {
            $("#bulan_pil").selectpicker("destroy");
            $("#bulan_pil").html(null);
        },
        success: function (res) {
            console.log(res);
            if (res.transaction) {
                $("#bulan_pil").append('<option value="">All Bulan</option>');
                $.each(res.data.bulan, function (k, v) {
                    $("#bulan_pil").append('<option value="' + v.bulan_teks + '">' + v.bulan_teks + '</option>');
                });
                $("#bulan_pil").selectpicker();
            }
        }
    });

}

function getProvinsi() {

    $.ajax({
        url: "/bigenvelope/api/call/id/331",
        method: "post",
        data: {
            item: 'provinsi',
            tahun_pil: $("#tahun_pil").val(),
            bulan_pil: $("#bulan_pil").val(),
        },
        beforeSend: function () {
            $("#provinsi_pil").selectpicker("destroy");
            $("#provinsi_pil").html(null);
        },
        success: function (res) {
            if (res.transaction) {
                $("#provinsi_pil").append('<option value="">All Provinsi</option>');
                $.each(res.data.provinsi, function (k, v) {
                    $("#provinsi_pil").append('<option value="' + v.nama_prov + '">' + v.nama_prov + '</option>');
                });
                $("#provinsi_pil").selectpicker();
            }
        }
    });

}

function getKabupaten() {

    $.ajax({
        url: "/bigenvelope/api/call/id/331",
        method: "post",
        data: {
            item: 'kabupaten',
            tahun_pil: $("#tahun_pil").val(),
            bulan_pil: $("#bulan_pil").val(),
            provinsi_pil: $("#provinsi_pil").val()
        },
        beforeSend: function () {
            $("#kab_pil").selectpicker("destroy");
            $("#kab_pil").html(null);
        },
        success: function (res) {
            if (res.transaction) {
                $("#kab_pil").append('<option value="">All Kabupaten/Kota</option>');
                $.each(res.data.kabupaten, function (k, v) {
                    $("#kab_pil").append('<option value="' + v.nama_kab + '">' + v.nama_kab + '</option>');
                });
                $("#kab_pil").selectpicker();
            }
        }
    });

}