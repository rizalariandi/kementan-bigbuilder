var zoom_view = 4.32;
var lft_map_harga_harian; // openstreetmap tile untuk harga komoditas harian
var json_data_konsumen;
var json_data_produsen;
var layergroup;
var jenis_produk = "";
var prov_select_konsm = "";
var date_ = "";
var tanggal_produsen = "";
var tanggal_konsumen = "";
var polygon_konsumen = []; // simpan geojson untuk json_Data, struktur => [konsumen_prov, konsumen_kab]
let url_server_prov = "/assets/kementan/geojson_kementan/DB_PROP.json";
let url_server_kabkot = "/assets/kementan/geojson_kementan/DB_KAB.json";
let src_produsen_nasional = "https://10.1.231.160:8443/public/dashboard/1bc299e9-f722-4a55-8d74-0b08b2974ade";
let src_produsen_provinsi = "https://10.1.231.160:8443/public/dashboard/d8701ae8-740b-4fa8-a072-72c6f744bfa1"; // url + ?tanggal=2020-11-05&provinsi=JAWA%20TENGAH
let src_konsumen_nasional = "https://10.1.231.160:8443/public/dashboard/24a002b6-80dc-4275-87b4-08fef87768d8";
let src_konsumen_provinsi = "https://10.1.231.160:8443/public/dashboard/a0a66470-10c0-4b70-852c-0774e0cb6eed";
let src_konsumen_kabupaten = "https://10.1.231.160:8443/public/dashboard/ab3dfacd-12a0-4c64-b21e-7eb7c295d603";
let src_graph_produsen = "https://10.1.231.160:8443/public/dashboard/c60387ae-2071-447f-ae6b-81c468e096a3";
let src_graph_konsumen = "https://10.1.231.160:8443/public/dashboard/7d1e81c0-ed4b-491b-ac29-beeaa7205e81";
const default_coord1 = ["Center", -2.600029, 118.015776];
var select_input = []; // isi array selalu: date, prov, kabkot atau date, kabkot, prov

let highlight_prod_color = {
    fillColor: 'red',
    weight: 2,
    opacity: 1,
    color: '#cd0a0a', //Outline color
    fillOpacity: 0.3
};

$(function () {
    $(".m-selectpicker").selectpicker();
    datePicker();

    initMap();

    let tmp_tgl = tanggal_konsumen; //getCurrentDate("konsumen");
    select_input.push("?tanggal=" + tmp_tgl);
    console.log("tgl = " + select_input);

    $("#iframe-prod").attr("src", src_produsen_nasional + "?tanggal=" + getCurrentDate("produsen"));
    $("#iframe-kon").attr("src", src_konsumen_nasional + "?tanggal=" + getCurrentDate("konsumen"));

    // let curdate = getCurrentDate();
    $("#provinsi_produsen").on("change", changeProvinsiProdusen);
    $("#provinsi_konsumen").on("change", changeProvinsiKonsumen);
    $("#kabupaten_konsumen").on("change", changeKabupaten);
    $("#jenis-graph").on("change", graphSelector);
    $("#jenis-produk").on("change", jenisProduk);
})

function initMap() {

    // leaflet init: id_html -> unntuk selector id tag di html
    // set_view -> untuk assign koordinat default ketika map di-load
    // zoom -> untuk set zoom level default ketika map di-load
    lft_map_harga_harian = L.map('map-highlight-harga-harian', {
        minZoom: zoom_view
    }).setView([default_coord1[1], default_coord1[2]], zoom_view);

    // OpenStreetMap init
    const OpenStreetMapUrl = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";

    const OpenStreetMapTileHargaHarian = L.tileLayer(OpenStreetMapUrl, {
        minZoom: 1,
        maxZoom: 20,
        attribution: "<a href='http://www.openstreetmap.org'> &copy;OpenStreetMap </a>, <a href='http://www.pertanian.go.id'> Kementan </a>"
    });

    OpenStreetMapTileHargaHarian.addTo(lft_map_harga_harian);

    // tambah layer untuk highlight
    layergroup = new L.FeatureGroup().addTo(lft_map_harga_harian);
    json_data_konsumen = L.geoJSON().addTo(layergroup);
    json_data_produsen = L.geoJSON('', {
        style: highlight_prod_color
    }).addTo(layergroup);
}

function deleteSelectInput(limit) {
    while (select_input.length > limit) {
        select_input.pop();
    }
}

function datePicker() {
    var date = {};

    $("#kt_daterangepicker_2").daterangepicker({
            autoApply: true,
            startDate: moment().subtract(30, 'days'),
            endDate: moment(),
            maxDate: moment()
        },
        function (start, end) {
            date = {
                'start_date': start.format('YYYY-MM-DD'),
                'end_Date': end.format('YYYY-MM-DD')
            }
            $("#txt-date").val(date.start_date + " hingga " + date.end_Date);
            date_ = "tanggal=" + date.start_date + "~" + date.end_Date
            changeIframe();
            // console.log(date);
        }
    );

    $("#tanggal_produsen").daterangepicker({
            autoApply: true,
            singleDatePicker: true,
            showDropdowns: true,
            maxDate: moment()
        },
        function () {}
    );

    $("#tanggal_kosumen").daterangepicker({
            autoApply: true,
            singleDatePicker: true,
            showDropdowns: true,
            maxDate: moment()
        },
        function () {}
    );

    date = {
        'start_date': moment().subtract(30, 'days').format('YYYY-MM-DD'),
        'end_Date': moment().format('YYYY-MM-DD')
    }
    $("#txt-date").val(date.start_date + " hingga " + date.end_Date);
    date_ = "tanggal=" + date.start_date + "~" + date.end_Date
    changeIframe();
}

function getCurrentDate(kon_prod) {
    // var month = new Array();
    // month[0] = "01"; // january
    // month[1] = "02"; // february
    // month[2] = "03"; // march
    // month[3] = "04"; // april
    // month[4] = "05"; // may
    // month[5] = "06"; // june
    // month[6] = "07"; // july
    // month[7] = "08"; // august
    // month[8] = "09"; // september
    // month[9] = "10"; // october
    // month[10] = "11"; // november
    // month[11] = "12"; // december

    // var date = new Date();
    // console.log(date.getFullYear() + "-" + month[date.getMonth()] + "-" + date.getDate());
    // return date.getFullYear() + "-" + month[date.getMonth()] + "-" + date.getDate();
    if (kon_prod == "produsen") {
        tanggal_produsen = $('#tanggal_produsen').text();
        return $('#tanggal_produsen').text();
    } else if (kon_prod == "konsumen") {
        tanggal_konsumen = $('#tanggal_konsumen').text();
        return $('#tanggal_konsumen').text();
    }
}

function searchGeoJsonProdusen(url, administratif, searchDaerah) {
    $.ajax({
        url: url,
        dataType: "json",
        success: function (res) {
            console.log("ajax masuk", res);
            var mark = 0;
            $.each(res.features, function (key, val) {
                if (administratif === "provinsi") {
                    if (val.properties['IDADMIN'] == searchDaerah) {
                        console.log("masuk search prov");
                        mark = 1;

                        var geo_data = {
                            "type": "Feature",
                            "properties": {
                                "color": "red"
                            },
                            "geometry": {
                                "type": val.geometry['type'],
                                "coordinates": val.geometry['coordinates']
                            }
                        };
                        if (json_data_produsen) {
                            json_data_produsen.clearLayers();
                            json_data_produsen.addData(geo_data);
                            lft_map_harga_harian.fitBounds(layergroup.getBounds());
                        }
                    }
                }
            })
            if (mark === 0) {
                toastr["error"]("Geometry map data not found", "Alert!!");

                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                json_data_produsen.clearLayers();
                lft_map_harga_harian.setView([default_coord1[1], default_coord1[2]], zoom_view);
            }
        },
        error: function (xhr, status, error) {
            alert("search geojson ajax gagal");
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
}

function evaluateMapKonsumen(geoData, search_val, administratif) {
    if (polygon_konsumen.length == 0) {
        polygon_konsumen.push(geoData);
        json_data_konsumen.clearLayers();
        json_data_konsumen.addData(geoData);
        lft_map_harga_harian.fitBounds(layergroup.getBounds());
    } else if (polygon_konsumen.length == 1) {
        if (search_val == "") {
            polygon_konsumen.splice(0, 1);
            json_data_konsumen.clearLayers();
            lft_map_harga_harian.fitBounds(layergroup.getBounds());
        } else {
            if (polygon_konsumen[0].properties.administrative === administratif) {
                polygon_konsumen[0] = geoData;
                json_data_konsumen.clearLayers();
                json_data_konsumen.addData(polygon_konsumen[0]);
                lft_map_harga_harian.fitBounds(layergroup.getBounds());
            } else {
                polygon_konsumen.push(geoData);
                if (polygon_konsumen[0].properties.administrative == "kab/kota" &&
                    polygon_konsumen[1].properties.administrative == "provinsi") {
                    json_data_konsumen.clearLayers();
                    json_data_konsumen.addData(polygon_konsumen[0]);
                    lft_map_harga_harian.fitBounds(layergroup.getBounds());
                } else if (polygon_konsumen[1].properties.administrative == "kab/kota" &&
                    polygon_konsumen[0].properties.administrative == "provinsi") {
                    json_data_konsumen.clearLayers();
                    json_data_konsumen.addData(polygon_konsumen[1]);
                    lft_map_harga_harian.fitBounds(layergroup.getBounds());
                }
            }
        }
    } else if (polygon_konsumen.length == 2) {
        if (search_val == "") {
            if (polygon_konsumen[0].properties.administrative === administratif) {
                polygon_konsumen.splice(0, 1);
            } else if (polygon_konsumen[1].properties.administrative === administratif) {
                polygon_konsumen.splice(1, 1);
            }
            json_data_konsumen.clearLayers();
            json_data_konsumen.addData(polygon_konsumen, {
                style: highlight_kon_color
            });
            lft_map_harga_harian.fitBounds(layergroup.getBounds());
        } else {
            if (polygon_konsumen[0].properties.administrative === administratif) {
                polygon_konsumen[0] = geoData;
            } else if (polygon_konsumen[1].properties.administrative === administratif) {
                polygon_konsumen[1] = geoData;
            }

            if (administratif === "provinsi") {
                if (polygon_konsumen[0].properties.administrative == "kab/kota") {
                    polygon_konsumen.splice(0, 1);
                } else if (polygon_konsumen[1].properties.administrative == "kab/kota") {
                    polygon_konsumen.splice(1, 1);
                }
                json_data_konsumen.clearLayers();
                json_data_konsumen.addData(polygon_konsumen);
                lft_map_harga_harian.fitBounds(layergroup.getBounds());
            } else if (administratif === "kab/kota") {
                if (polygon_konsumen[0].properties.administrative == "kab/kota" &&
                    polygon_konsumen[1].properties.administrative == "provinsi") {
                    json_data_konsumen.clearLayers();
                    json_data_konsumen.addData(polygon_konsumen[0]);
                    lft_map_harga_harian.fitBounds(layergroup.getBounds());
                } else if (polygon_konsumen[1].properties.administrative == "kab/kota" &&
                    polygon_konsumen[0].properties.administrative == "provinsi") {
                    json_data_konsumen.clearLayers();
                    json_data_konsumen.addData(polygon_konsumen[1]);
                    lft_map_harga_harian.fitBounds(layergroup.getBounds());
                }
            }
        }
    }
}

function searchGeoJsonKonsumen(url, administratif, searchDaerah) {
    $.ajax({
        url: url,
        dataType: "json",
        success: function (res) {
            console.log("ajax masuk", res);
            var mark = 0;
            var geo_data = {};
            $.each(res.features, function (key, val) {
                if (administratif === "provinsi") {
                    if (val.properties['IDADMIN'] == searchDaerah) {
                        console.log("masuk search prov");
                        mark = 1;
                        geo_data = {
                            "type": "Feature",
                            "properties": {
                                "administrative": administratif
                            },
                            "geometry": {
                                "type": val.geometry['type'],
                                "coordinates": val.geometry['coordinates']
                            }
                        };
                    }
                } else if (administratif === "kab/kota") {
                    if (val.properties['idadmin'] == searchDaerah) {
                        console.log("masuk search kabkot");
                        mark = 1;
                        geo_data = {
                            "type": "Feature",
                            "properties": {
                                "administrative": administratif
                            },
                            "geometry": {
                                "type": val.geometry['type'],
                                "coordinates": val.geometry['coordinates']
                            }
                        };
                    }
                }
            })
            if (mark === 0) {
                toastr["error"]("Geometry map data not found", "Alert!!");

                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            } else {
                if (json_data_konsumen && geo_data) {
                    evaluateMapKonsumen(geo_data, searchDaerah, administratif);
                }
            }
        },
        error: function (xhr, status, error) {
            alert("search geojson ajax gagal");
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
}

function highlightMap(kodeDaerahDB, jenisIframe, administratif) {
    if (kodeDaerahDB != "") {
        var tmp = kodeDaerahDB;

        if (jenisIframe === "konsumen") {
            if (administratif == "provinsi") {
                // let search = tmp.join(" ");
                searchGeoJsonKonsumen(url_server_prov, "provinsi", tmp);
            } else if (administratif == "kab/kota") {
                // let search = tmp.slice(1, tmp.length).join(" ");
                searchGeoJsonKonsumen(url_server_kabkot, "kab/kota", tmp);
            }
        } else if (jenisIframe === "produsen") {
            if (administratif == "provinsi") {
                // let search = tmp.join(" ");
                searchGeoJsonProdusen(url_server_prov, "provinsi", tmp);
            }
        }
    } else {
        if (jenisIframe === "produsen") {
            json_data_produsen ? json_data_produsen.clearLayers() : '';
            lft_map_harga_harian.fitBounds(layergroup.getBounds());
        } else if (jenisIframe === "konsumen") {
            json_data_konsumen ? json_data_konsumen.clearLayers() : '';
            lft_map_harga_harian.fitBounds(layergroup.getBounds());
        }
    }
}

async function changeProvinsiProdusen() {
    let tmp_prov = $("#provinsi_produsen").val().split(",");
    // console.log("tmp_prov = ",x);
    var prov_select;
    var prov_kode;
    if (tmp_prov.length == 1 && tmp_prov[0] == "") {
        prov_select = "";
        prov_kode = "";
    } else if (tmp_prov.length == 2) {
        prov_select = tmp_prov[1]
        prov_kode = tmp_prov[0];
    }
    // let prov_select = tmp_prov[1].split(" ").join("%20");
    // let prov_kode = tmp_prov[0];


    if (prov_select == "") {
        const result = await getMaxDateProdusen("", "");
        if (result) {
            var date = tanggal_produsen //getCurrentDate("produsen");
            $("#iframe-prod").attr("src", src_produsen_nasional + "?tanggal=" + date);
        }
    } else {
        const result = await getMaxDateProdusen("provinsi", prov_select);
        if (result) {
            var date = tanggal_produsen //getCurrentDate("produsen");
            let new_src_prod_prov = src_produsen_provinsi + "?tanggal=" + date + "&provinsi=" + prov_select;
            console.log("new_src_prod_prov = ", new_src_prod_prov);
            $("#iframe-prod").attr("src", new_src_prod_prov);
        }
    }
    highlightMap(prov_kode, 'produsen', 'provinsi');
}

function graphSelector() {
    let graph_select = $("#jenis-graph").val();

    // if (graph_select === "produsen") {
    //     $("#portlet-691").attr("src", src_graph_produsen);
    // } else if (graph_select === "konsumen") {
    //     $("#portlet-691").attr("src", src_graph_konsumen);
    // }
    jenis_produk = "";
    changeIframe();
    $.ajax({
        url: "/bigenvelope/api/call/id/2585",
        method: "post",
        data: {
            harga: graph_select,
        },
        beforeSend: function () {
            $("#jenis-produk").selectpicker("destroy");
            $("#jenis-produk").html(null);
        },
        success: function (res) {
            if (res.transaction) {
                $.each(res.data, function (k, v) {
                    $("#jenis-produk").append('<option value="' + v.nama_komoditas + '">' + v.nama_komoditas + '</option>');
                });
                $("#jenis-produk").selectpicker();
            }
        }
    });
}

function jenisProduk() {
    let array_produk = $("#jenis-produk").val();
    jenis_produk = "";
    array_produk.forEach(element => jenis_produk += "produk=" + element + "&");
    changeIframe();
}

function changeIframe() {
    let graph_select = $("#jenis-graph").val();
    var con = jenis_produk || date_ ? '?' : '';
    jenis_produk = jenis_produk ? jenis_produk : "produk=xxx&";
    if (graph_select === "produsen") {
        $("#iframe-grafik").attr("src", src_graph_produsen + con + jenis_produk + date_);
    } else if (graph_select === "konsumen") {
        $("#iframe-grafik").attr("src", src_graph_konsumen + con + jenis_produk + date_);
    }
}

async function changeProvinsiKonsumen() {
    getKabupaten();

    // console.log("isi prov_2 => ", $("#provinsi_2").val());
    var tmp_prov_kon = $("#provinsi_konsumen").val().split(",");
    console.log("tmp_prov_kon = ", tmp_prov_kon);

    prov_select_konsm;
    var prov_kode;
    if (tmp_prov_kon.length == 1 && tmp_prov_kon[0] == "") {
        prov_select_konsm = "";
        prov_kode = "";
    } else if (tmp_prov_kon.length == 2) {
        prov_select_konsm = tmp_prov_kon[1]
        prov_kode = tmp_prov_kon[0];
    }

    // let tmp_prov = $("#provinsi_konsumen").val().split(" ");
    // let prov_select_konsm = tmp_prov.join("%20");

    if (prov_select_konsm == "") {
        result = await getMaxDateKonsumen("", "", "");
        if (result) {
            $("#iframe-kon").attr("src", src_konsumen_nasional + "?tanggal=" + tanggal_konsumen);
        }
    } else {
        result = await getMaxDateKonsumen("provinsi", prov_select_konsm, "");
        if (result) {
            addon = ""
            addon = tanggal_konsumen || prov_select_konsm || kab_select ? "?" : ""
            addon += tanggal_konsumen ? "tanggal=" + tanggal_konsumen + "&" : "";
            addon += prov_select_konsm ? "provinsi=" + prov_select_konsm + "&" : "";            
            $("#iframe-kon").attr("src", src_konsumen_provinsi + addon);
        }
    }
    console.log(select_input);

    highlightMap(prov_kode, 'konsumen', 'provinsi');
}

async function changeKabupaten() {

    let tmp_kab = $("#kabupaten_konsumen").val().split(",");
    console.log("tmp_kab = ", tmp_kab);

    var kab_select;
    var kab_kode;
    if (tmp_kab.length == 1 && tmp_kab[0] == "") {
        kab_select = "";
        kab_kode = "";
    } else if (tmp_kab.length == 2) {
        kab_select = tmp_kab[1];
        kab_kode = tmp_kab[0];
    }
    // let tmp_kab = $("#kabupaten_konsumen").val().split(" ");
    // let kab_select = tmp_kab.join("%20");
    result = await getMaxDateKonsumen('kabupaten', prov_select_konsm, kab_select);
    if (result) {
        addon = ""
        addon = tanggal_konsumen || prov_select_konsm || kab_select ? "?" : ""
        addon += tanggal_konsumen ? "tanggal=" + tanggal_konsumen + "&" : "";
        addon += prov_select_konsm ? "provinsi=" + prov_select_konsm + "&" : "";
        addon += kab_select ? "kabupaten_kota=" + kab_select + "&" : "";
        url = kab_select ? src_konsumen_kabupaten : src_konsumen_provinsi;
        $("#iframe-kon").attr("src", url + addon);

        highlightMap(kab_kode, 'konsumen', 'kab/kota');
    }
}



function getKabupaten() { // ini masih gatau cara panggil db nya biar kabupaten yang muncul akan sesuai provinsi yang dipilih

    $.ajax({
        url: "/bigenvelope/api/call/id/336",
        method: "post",
        data: {
            nama_provinsi: $("#provinsi_konsumen").val().split(",")[1],
            item: 'kabupaten'
        },
        beforeSend: function () {
            $("#kabupaten_konsumen").selectpicker("destroy");
            $("#kabupaten_konsumen").html(null);
        },
        success: function (res) {
            if (res.transaction) {
                $("#kabupaten_konsumen").append('<option value="">All Kabupaten/Kota</option>');
                $.each(res.data.kabupaten, function (k, v) {
                    $("#kabupaten_konsumen").append('<option value="' + v.regencies_id + "," + v.nama_kabupaten + '">' + v.nama_kabupaten + '</option>');
                });
                $("#kabupaten_konsumen").selectpicker();
            }
        }
    });

}

function getMaxDateKonsumen(items, prov_select, kabupaten) {
    return new Promise(resolve => {
        $.ajax({
            url: "/bigenvelope/api/call/id/2599",
            method: "post",
            data: {
                item: items,
                provinsi: prov_select,
                kabupaten: kabupaten
            },
            success: function (res) {
                if (res.transaction) {
                    if ((items == "provinsi" || items == "kabupaten") && prov_select && !kabupaten) {
                        tanggal_konsumen = res["data"]["tanggal"]["tanggal_max_prov"];
                        $("#tanggal_konsumen").text(res["data"]["tanggal"]["tanggal_max_prov"]);
                    } else if (prov_select || kabupaten) {
                        tanggal_konsumen = res["data"]["tanggal"]["tanggal_max_kab"];
                        $("#tanggal_konsumen").text(res["data"]["tanggal"]["tanggal_max_kab"]);
                    } else if (!prov_select && !kabupaten) {
                        tanggal_konsumen = res["data"]["tanggal"]["tanggal_max_nas"];
                        $("#tanggal_konsumen").text(res["data"]["tanggal"]["tanggal_max_nas"]);
                    }
                    // $("#tanggal_konsumen").html(res["data"]["tanggal"]["tanggal_max_nas"]);
                }
                resolve(true);
            },
            error: function () {
                resolve(true);
                console.log("getMaxDateKonsumen gagal");
            }
        });
    });
}

function getMaxDateProdusen(items, prov_select) {
    return new Promise(resolve => {
        $.ajax({
            url: "/bigenvelope/api/call/id/2600",
            method: "post",
            data: {
                item: items,
                provinsi: prov_select
            },
            success: function (res) {
                if (res.transaction) {
                    if (items == "provinsi") {
                        tanggal_produsen = res["data"]["tanggal"]["tanggal_max_prov"];
                        $("#tanggal_produsen").text(res["data"]["tanggal"]["tanggal_max_prov"]);
                    } else if (items == "") {
                        tanggal_produsen = res["data"]["tanggal"]["tanggal_max_nas"];
                        $("#tanggal_produsen").text(res["data"]["tanggal"]["tanggal_max_nas"]);
                    }
                }
                resolve(true);
            },
            error: function () {
                resolve(true);
                console.log("getMaxDateKonsumen gagal");
            }
        });
    });
}