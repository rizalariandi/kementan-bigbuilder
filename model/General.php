<?php

class Kementan_Model_General extends Zend_Db_Table_Abstract
{
    function getProvinsi($id = 0)
    {

        try {

            $kd_prov = (int) ($_POST["kd_prov"] ?? 0);

            $where = [];
            $sWhere = "";
            if ($kd_prov) {
                $where["nm_prov"] = $kd_prov;
                $sWhere .= " and nm_prov = ?";
            }

            $sql = "SELECT nm_prov,kd_prov from dm_harga_pangan_strategis_prov where nm_prov  is not null" . $sWhere . " GROUP BY nm_prov, kd_prov";
            $data = $this->_db->fetchAll($sql, $where);
            // Zend_Debug::dump($data);die();

            return [
                "transaction" => true,
                "data" => $data
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();

            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
        }
    }

    function getKabupaten()
    {

        try {

            // Zend_Debug::dump($_POST);die();

            $kd_kab = (int) ($_POST["kd_kab"] ?? 0);
            $kd_prov = $_POST["kd_prov"] ?? 0;

            $where = [];
            $sWhere = "";
            if ($kd_kab > 0) {
                $where[] = $kd_kab;
                $sWhere .= " and regencies_id = ?";
            }
            if ($kd_prov) {
                $where[] = $kd_prov;
                $sWhere .= " and provinces_nm = ?";
            }
            $sql = "SELECT DISTINCT regencies_id kd_kab, regencies_nm nm_kab from dm_harga_pangan_strategis_kab  where 1=1 and regencies_nm  is not null " . $sWhere;
            $data = $this->_db->fetchAll($sql, $where);
            // Zend_Debug::dump($data);die();

            return [
                "transaction" => true,
                "data" => $data
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
            Zend_Debug::dump("Something Error");
            die();
        }
    }

    function getKecamatan()
    {

        try {

            // Zend_Debug::dump($_POST);die();

            $kd_kec = (int) ($_POST["kd_kec"] ?? 0);
            $kd_kab = (int) ($_POST["kd_kab"] ?? 0);
            $kd_prov = (int) ($_POST["kd_prov"] ?? 0);

            $where = [];
            $sWhere = "";
            if ($kd_kec > 0) {
                $where[] = $kd_kec;
                $sWhere .= " and kd_kec = ?";
            }
            if ($kd_kab > 0) {
                $where[] = $kd_kab;
                $sWhere .= " and kec_kd_kab = ?";
            }
            if ($kd_prov > 0) {
                $where[] = $kd_prov;
                $sWhere .= " and kec_kd_prov = ?";
            }

            $sql = "SELECT kd_kec, nm_kec from tblm_kec where 1=1" . $sWhere;
            $data = $this->_db->fetchAll($sql, $where);
            // Zend_Debug::dump($data);die();

            return [
                "transaction" => true,
                "data" => $data
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
            Zend_Debug::dump("Something Error");
            die();
        }
    }

    function getMaxDateStrategisProdusen($id = 0)
    {

        try {
            $where = "";
            if (!$_POST["item"]) {
                $sql = "SELECT max(TANGGAL) as tanggal from dm_harga_pangan_strategis_nas where jenis ='Harga Produsen'";
                $data = $this->_db->fetchAll($sql, $where);
            } else if ($_POST["item"] == 'provinsi') {
                $sql = "SELECT max(TANGGAL) as tanggal from dm_harga_pangan_strategis_prov where jenis ='Harga Produsen'";
                $data = $this->_db->fetchAll($sql, $where);
            }

            // Zend_Debug::dump($data);die();

            return [
                "transaction" => true,
                "data" => $data[0]
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();

            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
        }
    }

    function getMaxDateStrategisKonsumen($id = 0)
    {

        try {
            $where = "";
            if (!$_POST["item"]) {
                $sql = "SELECT max(TANGGAL) as tanggal from dm_harga_pangan_strategis_prov where jenis ='Harga Konsumen'";
                $data = $this->_db->fetchAll($sql, $where);
            } else if ($_POST["item"] == 'provinsi') {
                $sql = "SELECT max(TANGGAL) as tanggal from dm_harga_pangan_strategis_prov where jenis ='Harga Konsumen'";
                $data = $this->_db->fetchAll($sql, $where);
            } else if ($_POST["item"] == 'kabupaten') {
                $sql = "SELECT max(TANGGAL) as tanggal from dm_harga_pangan_strategis_kab where jenis ='Harga Konsumen'";
                $data = $this->_db->fetchAll($sql, $where);
            }
            // Zend_Debug::dump($data);die();

            return [
                "transaction" => true,
                "data" => $data[0]
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();

            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
        }
    }

    function getMaxDateCipinangJakarta($id = 0)
    {

        try {
            $where = "";
            $sql = "SELECT max(TANGGAL) as tanggal from dm_stock_beras_cipinang";
            $data = $this->_db->fetchAll($sql, $where);
            // Zend_Debug::dump($data);die();

            return [
                "transaction" => true,
                "data" => $data[0]
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();

            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
        }
    }

    function getMaxDateStrategisBulog($id = 0)
    {

        try {
            $where = "";
            $sql = "SELECT max(TANGGAL) as tanggal from dm_stock_komoditas_bulog";
            $data = $this->_db->fetchAll($sql, $where);
            // Zend_Debug::dump($data);die();

            return [
                "transaction" => true,
                "data" => $data[0]
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();

            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
        }
    }

    function getOptionFilterPIHC($id = 0)
    {
        try {
            // $where = "";
            $tahun = "";
            $bulan = "";
            $provinsi = "";
            $kabupaten = "";
            $sWhere  = '';
            $where = [];
            $tahun_pil = $_POST["tahun_pil"];
            $sWhere  = $_POST['tahun_pil'] || $_POST['bulan_pil'] || $_POST['provinsi_pil'] ? ' where ' : '';
            $sWhere  .= $_POST['tahun_pil'] ? 'tahun = ?' : '';
            $sWhere  .= $_POST['bulan_pil'] ? ($_POST['tahun_pil'] ? ' and bulan_teks = ?' : 'bulan_teks = ?') : '';
            $sWhere  .= $_POST['provinsi_pil'] ? ($_POST['tahun_pil'] || $_POST['bulan_pil'] ? ' and nama_prov = ?' : 'nama_prov = ?') : '';
            if ($_POST["tahun_pil"]) {
                $where[] = $tahun_pil;
            }
            if ($_POST['bulan_pil']) {
                $where[] = $_POST['bulan_pil'];
            }
            if ($_POST['provinsi_pil']) {
                $where[]  = $_POST['provinsi_pil'];
            }
            // Zend_Debug::dump($where);
            // die();
            if ($_POST["item"] == 'tahun' ||  !$_POST["item"]) {

                $sql = "SELECT distinct tahun from dm_pupuk_pihc";
                $tahun = $this->_db->fetchAll($sql, $where);
            }
            if ($_POST["item"] == 'bulan' ||  !$_POST["item"]) {
                $sql = "SELECT distinct bulan_teks from dm_pupuk_pihc" . $sWhere;
                $bulan = $this->_db->fetchAll($sql, $where);
            }
            if ($_POST["item"] == 'provinsi' ||  !$_POST["item"]) {
                $sql = "SELECT distinct nama_prov from dm_pupuk_pihc" . $sWhere;
                $provinsi = $this->_db->fetchAll($sql, $where);
            }
            if ($_POST["item"] == 'kabupaten' ||  !$_POST["item"]) {
                $sql = "SELECT distinct nama_kab from dm_pupuk_pihc" . $sWhere;
                $kabupaten = $this->_db->fetchAll($sql, $where);
            }
            // Zend_Debug::dump($data);die();

            return [
                "transaction" => true,
                "data" => [
                    "tahun" => $tahun,
                    "bulan" => $bulan,
                    "provinsi" => $provinsi,
                    "kabupaten" => $kabupaten
                ]
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();

            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
        }
    }

    function getOptionFiterKelahiranSapiKerbau($id = 0)
    {

        // die("ok");

        try {
            $sWhere  = '';
            $where = [];
            $sWhere  .= $_POST['startDate'] || $_POST['endDate'] || $_POST['provinsi'] ? ' where' : '';
            $sWhere  .= $_POST['provinsi'] ? ' provinsi = ?' : '';
            $sWhere  .= $_POST['startDate'] || $_POST['endDate'] ? ($_POST['provinsi'] ? ' and tanggal BETWEEN ? AND ?' : ' tanggal BETWEEN ? AND ?') : '';
            if ($_POST['provinsi']) {
                $where[]  = $_POST['provinsi'];
            }
            if ($_POST["startDate"]) {
                $where[] = $_POST['startDate'];
            }
            if ($_POST['endDate']) {
                $where[] = $_POST['endDate'];
            }
            if ($_POST["item"] == 'provinsi' ||  !$_POST["item"]) {
                $sql = "SELECT distinct provinsi from dm_kelahiran_sapi_kerbau" . $sWhere;
                $provinsi = $this->_db->fetchAll($sql, $where);
            }
            if ($_POST["item"] == 'kabupaten' ||  !$_POST["item"]) {
                $sql = "SELECT distinct kota from dm_kelahiran_sapi_kerbau" . $sWhere;
                $kota = $this->_db->fetchAll($sql, $where);
            }

            // Zend_Debug::dump($kota);
            // Zend_Debug::dump($provinsi);
            // die();

            return [
                "transaction" => true,
                "data" => [
                    "kota" => $kota,
                    "provinsi" => $provinsi
                ]
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();

            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
        }
    }

    function getOptionFiterPemotonganSapiKerbau($id = 0)
    {

        try {
            $sWhere  = '';
            $where = [];
            $sWhere  .= $_POST['startDate'] || $_POST['endDate'] || $_POST['provinsi'] ? ' where' : '';
            $sWhere  .= $_POST['provinsi'] ? ' provinsi = ?' : '';
            $sWhere  .= $_POST['startDate'] || $_POST['endDate'] ? ($_POST['provinsi'] ? ' and tanggal BETWEEN ? AND ?' : ' tanggal BETWEEN ? AND ?') : '';
            if ($_POST['provinsi']) {
                $where[]  = $_POST['provinsi'];
            }
            if ($_POST["startDate"]) {
                $where[] = $_POST['startDate'];
            }
            if ($_POST['endDate']) {
                $where[] = $_POST['endDate'];
            }
            if ($_POST["item"] == 'provinsi' ||  !$_POST["item"]) {
                $sql = "SELECT distinct provinsi from dm_pemotongan_sapi_kerbau" . $sWhere;
                $provinsi = $this->_db->fetchAll($sql, $where);
                // Zend_Debug::dump($sql);
                // die();
            }
            if ($_POST["item"] == 'kabupaten' ||  !$_POST["item"]) {
                $sql = "SELECT distinct kabupaten from dm_pemotongan_sapi_kerbau" . $sWhere;
                $kabupaten = $this->_db->fetchAll($sql, $where);
                // Zend_Debug::dump($sql);
                // die();
            }


            return [
                "transaction" => true,
                "data" => [
                    "kabupaten" => $kabupaten ? $kabupaten : '',
                    "provinsi" => $provinsi ? $provinsi : ''
                ]
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();

            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
        }
    }

    function getOptionFilterSebaranBenih($id = 0)
    {
        try {
            $sWhere  = '';
            $where = [];
            $tahun_pil = (int) ($_POST["tahun_pil"] ?? 0);
            $sWhere  = $_POST['tahun_pil'] || $_POST['bulan_pil'] ? ' where ' : '';
            $sWhere  .= $_POST['tahun_pil'] ? 'tahun = ?' : '';
            $sWhere  .= $_POST['bulan_pil'] ? ($_POST['tahun_pil'] ? ' and bulan_text = ?' : 'bulan_text = ?') : '';
            if ($_POST["tahun_pil"]) {
                $where[] = $tahun_pil;
            }
            if ($_POST['bulan_pil']) {
                $where[] = $_POST['bulan_pil'];
            }
            // Zend_Debug::dump($where);
            // die();
            if ($_POST["item"] == 'tahun' ||  !$_POST["item"]) {
                $sql = "SELECT distinct tahun from dm_sebaran_benih ";
                $tahun = $this->_db->fetchAll($sql, $where);
            }
            if ($_POST["item"] == 'bulan' ||  !$_POST["item"]) {
                $sql = "SELECT distinct bulan_text from dm_sebaran_benih" . $sWhere;
                $bulan = $this->_db->fetchAll($sql, $where);
            }
            if ($_POST["item"] == 'provinsi' ||  !$_POST["item"]) {
                $sql = "SELECT distinct provinsi from dm_sebaran_benih" . $sWhere;
                $provinsi = $this->_db->fetchAll($sql, $where);
            }
            return [
                "transaction" => true,
                "data" => [
                    "tahun" => $tahun ? $tahun : '',
                    "bulan" => $bulan ? $bulan : '',
                    "provinsi" => $provinsi ? $provinsi : ''
                ]
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();

            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
        }
    }

    function getMaxDateHarianKonsumen()
    {
        try {
            $where = "";            
            if ($_POST["provinsi"] && ($_POST["item"] == 'provinsi' || $_POST["item"] == 'kabupaten') && !$_POST["kabupaten"]) {
                $where_prov = $_POST["provinsi"] ? " where nama_provinsi = '" . $_POST["provinsi"] . "'" : "";
                $sql = "SELECT max(tanggal) tanggal_max_prov from dm_harga_komoditas_harian_konsumen_prov" . $where_prov;
                $tanggal =  $this->_db->fetchAll($sql, $where);
            }
             else if ($_POST["provinsi"] || $_POST["kabupaten"] ) {
                $where_kab = $_POST["provinsi"] || $_POST["kabupaten"] ? " where " : "";
                $where_kab .= $_POST["provinsi"] ? " nama_provinsi = '" . $_POST["provinsi"] . "'" : "";
                $where_kab .= $_POST["kabupaten"] ? ($_POST["provinsi"] ?  " and nama_kabupaten = '" . $_POST["kabupaten"] . "'" : " nama_kabupaten = '" . $_POST["kabupaten"] . "'") : "";
                $sql = "SELECT max(tanggal) tanggal_max_kab from dm_harga_komoditas_harian_konsumen_kab" . $where_kab;
                $tanggal =  $this->_db->fetchAll($sql, $where);
            }
            else if (!$_POST["provinsi"] && !$_POST["kabupaten"] ) {
                $sql = "SELECT max(tanggal) tanggal_max_nas from dm_harga_komoditas_harian_konsumen_nas";
                $tanggal =  $this->_db->fetchAll($sql, $where);
            } 

            return [
                "transaction" => true,
                "data" => [
                    "tanggal" => $tanggal[0]
                ]
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();

            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
        }
    }

    function getMaxDateHarianProdusen()
    {
        try {

            $where = "";
            if (!$_POST["item"]) {
                $sql = "SELECT max(tanggal) tanggal_max_nas from dm_harga_komoditas_harian_produsen_nas";
                $tanggal = $this->_db->fetchAll($sql, $where);
            } else if ($_POST["item"] == 'provinsi') {
                $where_prov = $_POST["provinsi"] ? " where nama_provinsi = '" . $_POST["provinsi"] . "'" : "";
                $sql = "SELECT max(tanggal) tanggal_max_prov from dm_harga_komoditas_harian_produsen_prov " . $where_prov;
                $tanggal = $this->_db->fetchAll($sql, $where);
            }

            return [
                "transaction" => true,
                "data" => [
                    "tanggal" => $tanggal[0]
                ]
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();

            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
        }
    }

    function getOptionFilterHarianProdusen($id = 0)
    {

        try {
            $where = "";
            $grup_prod = " group by nama_provinsi,provinces_id";
            $sql = "SELECT distinct nama_provinsi,provinces_id from dm_harga_komoditas_harian_produsen_prov" . $grup_prod;
            $provinsi = $this->_db->fetchAll($sql, $where);
            // Zend_Debug::dump($data);die();	        

            return [
                "transaction" => true,
                "data" => [
                    "provinsi" => $provinsi,
                    "tanggal" => $tanggal[0]
                ]
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();

            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
        }
    }

    function getOptionFilterHarianKosumen($id = 0)
    {

        try {
            $sWhere  = '';
            $where = [];
            $nama_provinsi = $_POST["nama_provinsi"] ?? 0;
            $sWhere  = $_POST['nama_provinsi'] ? ' where ' : '';
            $sWhere  .= $_POST['nama_provinsi'] ? 'nama_provinsi = ?' : '';
            if ($_POST["nama_provinsi"]) {
                $where[] = $nama_provinsi;
            }
            // Zend_Debug::dump($where);
            // die();
            if ($_POST["item"] == 'provinsi' ||  !$_POST["item"]) {
                $grup_1 = " group by nama_provinsi,provinces_id";
                $sql = "SELECT distinct nama_provinsi,provinces_id from dm_harga_komoditas_harian_konsumen_prov " . $grup_1;
                $provinsi = $this->_db->fetchAll($sql, $where);
            }
            if ($_POST["item"] == 'kabupaten' ||  !$_POST["item"]) {
                $grup_2 = " group by nama_kabupaten,regencies_id";
                $sql = "SELECT distinct nama_kabupaten,regencies_id from dm_harga_komoditas_harian_konsumen_kab " . $sWhere . $grup_2;
                $kabupaten = $this->_db->fetchAll($sql, $where);
            }
            // Zend_Debug::dump($data);die();

            return [
                "transaction" => true,
                "data" => [
                    "kabupaten" => $kabupaten,
                    "provinsi" => $provinsi
                ]
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();

            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
        }
    }

    function getFIlterKostraSimluh()
    {

        try {
            $sWhere  = '';
            $where = [];
            $sWhere  .= $_POST['tahun'] || $_POST['bulan'] || $_POST['provinsi'] || $_POST['kabupaten'] || $_POST['kecamatan'] ? ' where' : '';
            $sWhere  .= $_POST['tahun'] ? ' tahun = ?' : '';
            $sWhere  .= $_POST['bulan'] ? (strlen($sWhere) > 6 ? ' and bulan_text = ? ' : ' bulan_text = ?') : '';
            $sWhere  .= $_POST['provinsi'] ? (strlen($sWhere) > 6 ? ' and nm_prov = ? ' : ' nm_prov = ?') : '';
            $sWhere  .= $_POST['kabupaten'] ? (strlen($sWhere) > 6 ? ' and nm_kab = ? ' : ' nm_kab = ?') : '';
            $sWhere  .= $_POST['kecamatan'] ? (strlen($sWhere) > 6 ? ' and nm_kec = ? ' : ' nm_kec = ?') : '';
            if ($_POST['tahun']) {
                $where[]  = $_POST['tahun'];
            }
            if ($_POST["bulan"]) {
                $where[] = $_POST['bulan'];
            }
            if ($_POST['provinsi']) {
                $where[] = $_POST['provinsi'] ?? 0;
            }
            if ($_POST['kabupaten']) {
                $where[] = $_POST['kabupaten'] ?? 0;
            }
            if ($_POST['kecamatan']) {
                $where[] = $_POST['kecamatan'] ?? 0;
            }
            if (!$_POST["item"]) {
                $sort = " order by cast(tahun as unsigned)";
                $sql = "SELECT distinct tahun from dm_master_daerah_kostra_simluh where tahun is not null and  tahun > 0" . $sort;
                $tahun = $this->_db->fetchAll($sql, $where);
            }
            if ($_POST["item"] == 'tahun' ||  !$_POST["item"]) {
                $addon = strlen($sWhere) > 0 ? " and bulan_text is not null and  bulan_text <> '' " : " where bulan_text is not null and bulan_text <> '' ";
                $sort = " order by cast(bulan as unsigned)";
                $sql = "SELECT distinct bulan_text,bulan from dm_master_daerah_kostra_simluh" . $sWhere . $addon . $sort;
                $bulan = $this->_db->fetchAll($sql, $where);
            }
            if ($_POST["item"] == 'bulan' ||  !$_POST["item"]) {
                $sWhere_prov = !$sWhere ? " where kode_prov not like '%00' and concat('',kode_prov * 1) = kode_prov and kode_prov > 0" : $sWhere . " and kode_prov not like '%00' and concat('',kode_prov * 1) = kode_prov and kode_prov > 0";
                $sql = "SELECT  nm_prov,kode_prov from dm_master_daerah_kostra_simluh " . $sWhere_prov . " group by kode_prov , nm_prov";
                $provinsi = $this->_db->fetchAll($sql, $where);
            }
            if ($_POST["item"] == 'provinsi' ||  !$_POST["item"]) {
                $sWhere_kab = !$sWhere ? " where length(kode_kab) > 2 and nm_kab is not null and concat('',kode_kab * 1) = kode_kab" : $sWhere . " and length(kode_kab) > 2 and nm_kab is not null and concat('',kode_kab * 1) = kode_kab";
                $sql = "SELECT nm_kab,kode_kab from dm_master_daerah_kostra_simluh" . $sWhere_kab . " group by nm_kab,kode_kab";
                $kabupaten = $this->_db->fetchAll($sql, $where);
                // Zend_Debug::dump($data);die();
            }
            if ($_POST["item"] == 'kabupaten' ||  !$_POST["item"]) {
                $where_kec = !$sWhere ? " where concat('',kode_kec * 1) = kode_kec and kode_kec > 2 and nm_kec is not null" : $sWhere . " and concat('',kode_kec * 1) = kode_kec and kode_kec > 2 and nm_kec is not null";
                $sql = "SELECT nm_kec,kode_kec from dm_master_daerah_kostra_simluh" . $where_kec . " group by nm_kec , kode_kec";
                $kecamatan = $this->_db->fetchAll($sql, $where);
            }
            // Zend_Debug::dump($data);die();

            return [
                "transaction" => true,
                "data" => [
                    "tahun" => $tahun,
                    "bulan" => $bulan,
                    "provinsi" => $provinsi,
                    "kabupaten" => $kabupaten,
                    "kecamatan" => $kecamatan,
                ]
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();

            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
        }
    }

    function getProdakKomoditasHarian()
    {

        try {

            // Zend_Debug::dump($_POST);die();

            $harga = $_POST["harga"] ?? 0;

            $where = [];
            if ($harga == "produsen") {
                $sql = "SELECT DISTINCT nama_komoditas from dm_harga_komoditas_harian_produsen_nas";
                $data = $this->_db->fetchAll($sql, $where);
            } else if ($harga == 'konsumen') {
                $sql = "SELECT DISTINCT nama_komoditas from dm_harga_komoditas_harian_konsumen_nas";
                $data = $this->_db->fetchAll($sql, $where);
            }
            // Zend_Debug::dump($data);die();

            return [
                "transaction" => true,
                "data" => $data
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
            Zend_Debug::dump("Something Error");
            die();
        }
    }

    function getTahunAsuransi()
    {
        try {
            $where = [];
            $sql = "select distinct tahun from dm_asuransi_autp_summary_nilai_klaim
		union 
		select distinct tahun from dm_asuransi_auts_summary_nilai_klaim
		union 
		select distinct tahun from dm_asuransi_autp_summary_luas_lahan
		union 
		select distinct tahun from dm_asuransi_auts_summary_jumlah_sapi
		LIMIT 2000";
            $data = $this->_db->fetchAll($sql, $where);
            return [
                "transaction" => true,
                "data" => $data
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
            Zend_Debug::dump("Something Error");
            die();
        }
    }

    function getPersonalAsuransi()
    {
        $item = $_POST["item"] ?? 0;
        $search = $_POST["search"] ?? 0;
        $table = $_POST["autp_auts"] == 'autp' ? "dm_asuransi_autp_detail" : "dm_asuransi_auts_detail";
        if ($item == 'list') {
            $where = [];
            $sql = "select nik from $table where nik like '%$search%'";
            $data = $this->_db->fetchAll($sql, $where);
            return [
                "transaction" => true,
                "data" => $data
            ];
        } else {
            $where = [];
            $sql = "select * from $table where nik = '$search'";
            $data = $this->_db->fetchAll($sql, $where);
            return [
                "transaction" => true,
                "data" => $data[0]

            ];
        }
    }
    // masih on progress
    function getOptionFilterKUR($id = 0)
    {

        try {

            $sWhere  = '';
            $where = [];
            $tahun = $_POST["tahun"] ?? 0;
            $bulan = $_POST["bulan"] ?? 0;
            $sWhere  = $_POST['tahun'] || $_POST["bulan"] || $_POST["prov_kab"] ? ' where' : '';
            $sWhere  .= $_POST['tahun'] ? ' tahun = ?' : '';
            $sWhere  .= $_POST['bulan'] ? (strlen($sWhere) > 6  ? ' and periode = ?' : ' periode = ?') : '';
            if ($_POST["tahun"]) {
                $where[] = $tahun;
            }
            if ($_POST["bulan"]) {
                $where[] = $bulan;
            }
            // Zend_Debug::dump($where);
            // die();
            if ($_POST["item"] == 'tahun' ||  !$_POST["item"]) {
                $sort = "order by cast(tahun as unsigned)";
                $sql = "SELECT distinct tahun from dm_kur " . $sWhere . $sort;
                $tahun = $this->_db->fetchAll($sql, $where);
            }
            if ($_POST["item"] == 'bulan' ||  !$_POST["item"]) {
                $sql = "SELECT distinct periode from dm_kur " . $sWhere;
                $bulan = $this->_db->fetchAll($sql, $where);
            }
            if ($_POST["item"] == 'prov_kab' ||  !$_POST["item"]) {
                $sql = "SELECT distinct nama_wilayah,kode_wil from dm_kur " . $sWhere;
                $provkab = $this->_db->fetchAll($sql, $where);
            }
            return [
                "transaction" => true,
                "data" => [
                    "tahun" => $tahun,
                    "bulan" => $bulan,
                    "provkab" => $provkab
                ]
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();

            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
        }
    }

    function getFilterOPTDPI()
    {
        try {

            $sWhere  = '';
            $where = [];
            $tahun = $_POST["tahun"] ?? 0;
            $bulan = $_POST["bulan"] ?? 0;
            $provinsi = $_POST["provinsi"] ?? 0;
            $kabupaten = $_POST["kabupaten"] ?? 0;
            $sWhere  = $_POST['tahun'] || $_POST["bulan"] || $_POST["provinsi"] || $_POST["kabupaten"] ? ' where' : '';
            $sWhere  .= $_POST['tahun'] ? ' tahun = ?' : '';
            $sWhere  .= $_POST['bulan'] ? (strlen($sWhere) > 6  ? ' and bulan = ?' : ' bulan = ?') : '';
            $sWhere  .= $_POST['provinsi'] ? (strlen($sWhere) > 6  ? ' and nama_provinsi = ?' : ' nama_provinsi = ?') : '';
            if ($_POST["tahun"]) {
                $where[] = $tahun;
            }
            if ($_POST["bulan"]) {
                $where[] = $bulan;
            }
            if ($_POST["provinsi"]) {
                $where[] = $provinsi;
            }
            if ($_POST["kabupaten"]) {
                $where[] = $kabupaten;
            }
            // Zend_Debug::dump($where);
            // die();
            if ($_POST["item"] == 'tahun' ||  !$_POST["item"]) {
                $sort = "order by cast(tahun as unsigned)";                        // belum bisa sorting
                $sql = "SELECT distinct tahun from dm_master_daerah_opt_dpi " . $sWhere . $sort;
                $tahun = $this->_db->fetchAll($sql, $where);
            }
            if ($_POST["item"] == 'bulan' ||  !$_POST["item"]) {
                // $not_null = length( $swhere) > 6 ? "and bulan is not null and bulan_teks is not null" : "where bulan is not null and bulan_teks is not null";
                $not_null = " where bulan is not null and bulan_teks is not null";
                $grup = " group by bulan, bulan_teks";
                $sort = " order by cast(bulan as unsigned)";
                $sql = "SELECT distinct bulan,bulan_teks from dm_master_daerah_opt_dpi " . $sWhere . $not_null . $grup . $sort;
                $bulan = $this->_db->fetchAll($sql, $where);
            }
            if ($_POST["item"] == 'provinsi' ||  !$_POST["item"]) {
                $sWhere_prov = $sWhere ? $sWhere . " and kode_prov is not null" : " where kode_prov is not null";
                $sql = "select distinct kode_prov,nama_provinsi from dm_master_daerah_opt_dpi " . $sWhere_prov;
                $provinsi = $this->_db->fetchAll($sql, $where);
            }
            if ($_POST["item"] == 'kabupaten' ||  !$_POST["item"]) {
                $sWhere_kab = $sWhere ? $sWhere . " and kode_kab is not null" : " where kode_kab is not null";
                $sql = "select distinct kode_kab,nama_kab from dm_master_daerah_opt_dpi " . $sWhere_kab;
                $kabupaten = $this->_db->fetchAll($sql, $where);
            }
            if ($_POST["item"] == 'periode' ||  !$_POST["item"]) {
                $sWhere_periode = $sWhere ? $sWhere . " and periode is not null" : " where periode is not null";
                $sql = "select distinct periode from dm_master_daerah_opt_dpi " . $sWhere_periode;
                $periode = $this->_db->fetchAll($sql, $where);
            }
            return [
                "transaction" => true,
                "data" => [
                    "tahun" => $tahun,
                    "bulan" => $bulan,
                    "provinsi" => $provinsi,
                    "kabupaten" => $kabupaten,
                    "periode" => $periode
                ]
            ];
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();

            return [
                "transaction" => false,
                "message" => "Something Error"
            ];
        }
    }

    function __construct()
    {

        try {

            $con = new Bigenvelope_Model_Conn();
            $config = $con->getById(36);
            // Zend_Debug::dump($config);die();


            $params = array(
                'host' => $config["cparams"]["host"],
                'port' => $config["cparams"]["port"],
                'dbname' => $config["cparams"]["dbname"],
                'username' => $config["cparams"]["username"],
                'password' => $config["cparams"]["password"],
            );

            $this->_db = Zend_Db::factory($config["cparams"]["adapter"], $params);
            $this->_db->getConnection();
        } catch (Exception $e) {


            // return [
            //     "transaction" => false,
            //     "message" => "unable to connect to " . $config["conn_name"]
            // ];

            die("unable to connect to " . $config["conn_name"]);
            // Zend_Debug::dump($e->getMessage());die();
        }
    }
}
